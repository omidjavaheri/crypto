import numpy as np
from sklearn.preprocessing import StandardScaler
import tensorflow as tf
tf.compat.v1.disable_eager_execution()
from keras.layers.core import Dense, Dropout
from keras.layers.recurrent import LSTM
from keras.models import Sequential
from sklearn.model_selection import train_test_split

keras = tf.keras
from prediction.tuned.BaseModelForTuned import BaseModelForTuned
import os.path



class Lstm(BaseModelForTuned):
    def standard_scaler(self, X_train, X_test):
        train_samples, train_nx, train_ny = X_train.shape
        test_samples, test_nx, test_ny = X_test.shape

        X_train = X_train.reshape((train_samples, train_nx * train_ny))
        X_test = X_test.reshape((test_samples, test_nx * test_ny))

        scaler = StandardScaler()
        X_train = scaler.fit_transform(X_train)
        X_test = scaler.transform(X_test)

        X_train = X_train.reshape((train_samples, train_nx, train_ny))
        X_test = X_test.reshape((test_samples, test_nx, test_ny))

        return X_train, X_test

    def split_train_test(self):
        amount_of_features = len(self.data.columns)
        data = self.data.values
        window = int(self.params[0][0])
        sequence_length = window + 1
        result = []
        to_predict = []
        for index in range(len(data) - sequence_length):
            result.append(data[index: index + sequence_length])
        # we should minus window size but because we need same shapes to scale, we minus seq_length and then cut the firs later
        to_predict.append(data[len(data) - sequence_length: len(data)])


        result = np.array(result)
        to_predict = np.array(to_predict)

        result, to_predict = self.standard_scaler(result, to_predict)

        self.X_train = result[:, :-1]
        self.y_train = result[:, -1][:, -1]
        # remove first element that was added to prevent scaler error
        self.X_test = to_predict[:, 1:, :]

        self.X_train = np.reshape(self.X_train, (self.X_train.shape[0], self.X_train.shape[1], amount_of_features))
        self.X_test = np.reshape(self.X_test, (self.X_test.shape[0], self.X_test.shape[1], amount_of_features))

    def fit_and_predict(self):
        keras.backend.clear_session()
        tf.random.set_seed(42)
        np.random.seed(42)

        model = Sequential()
        model.add(LSTM(int(self.params[0][1]), input_shape=(self.X_train.shape[1], self.X_train.shape[2]), return_sequences=True))
        model.add(Dropout(float(self.params[0][2]), name="d0"))

        # second layer
        model.add(LSTM(int(self.params[0][1]), name="lstm_2"))
        model.add(Dropout(float(self.params[0][2]), name="d1"))

        model.add(Dense(1, name="final"))

        # self.X_train[np.isnan(self.X_train)] = 0
        XTraining, XValidation, YTraining, YValidation = train_test_split(self.X_train, self.y_train, test_size=0.1)  # before model building

        optimizer = self.params[0][3]
        model.compile(loss="mean_squared_error",
                      optimizer=optimizer)
        reset_states = ResetStatesCallback()
        model.fit(XTraining, YTraining, epochs=int(self.params[0][4]), batch_size=int(self.params[0][5]), verbose=0,
                  validation_data=(XValidation, YValidation),
                  callbacks=[reset_states])

        pred = model.predict(self.X_test)
        scaled_today_price = self.X_test[0, -1, self.price_index]
        scaled_tomorrow_price = pred[0, 0]
        tomorrow_percentage = ((scaled_tomorrow_price - scaled_today_price) / abs(scaled_today_price)) * 100
        today_price = self.data.iloc[-1]['price']
        return {'today_price': today_price, 'tomorrow_price': today_price + (today_price * (tomorrow_percentage / 100)),
                'tomorrow_percentage': tomorrow_percentage}


class ResetStatesCallback(keras.callbacks.Callback):
    def on_epoch_begin(self, epoch, logs):
        self.model.reset_states()




