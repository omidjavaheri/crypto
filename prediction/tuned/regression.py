from prediction.tuned.BaseModelForTuned import BaseModelForTuned
import numpy as np

from sklearn.linear_model import LinearRegression

class Regression(BaseModelForTuned):

    def fit_and_predict(self):
        regr = LinearRegression(fit_intercept=True)
        N = int(self.params[0][0])
        pred_min = 0
        i = self.data.shape[0]
        X_train = np.array(range(len(self.data['price'][i - N:i])))
        y_train = np.array(self.data['price'][i - N:i])
        X_train = X_train.reshape(-1, 1)
        y_train = y_train.reshape(-1, 1)
        regr.fit(X_train, y_train)
        pred = regr.predict(np.array(N).reshape(1, -1))
        pred = pred[0][0]

        # If the values are < pred_min, set it to be pred_min
        if pred < pred_min:
            pred = pred_min

        today_price = self.data.iloc[-1]['price']
        tomorrow_price = pred
        tomorrow_percentage = ((tomorrow_price - today_price) / abs(today_price)) * 100
        return {'today_price': today_price, 'tomorrow_price': tomorrow_price,
                'tomorrow_percentage': tomorrow_percentage}






