from prediction.tuned.BaseModelForTuned import BaseModelForTuned
import pandas as pd
from prediction.Helper import targets, features
from sklearn.preprocessing import StandardScaler
from xgboost import XGBRegressor


class MyBoost(BaseModelForTuned):

    def split_train_test(self):
        scaler = StandardScaler()
        data_scaled = scaler.fit_transform(self.data[:])
        # Convert the numpy array back into pandas dataframe
        data_scaled = pd.DataFrame(data_scaled, columns=self.data.columns)
        self.X_train = data_scaled[:-1][features]
        self.price_index = self.X_train.columns.get_loc('price')
        self.y_train = data_scaled[:-1][targets]
        self.X_test = (data_scaled.tail(1))[:][features]

    def fit_and_predict(self):
        n_estimators = int(self.params[0][0])  # Number of boosted trees to fit. default = 100
        max_depth = int(self.params[0][1])  # Maximum tree depth for base learners. default = 3
        learning_rate = float(self.params[0][2])  # Boosting learning rate (xgb’s “eta”). default = 0.1
        min_child_weight = int(self.params[0][3])  # Minimum sum of instance weight(hessian) needed in a child. default = 1
        subsample = float(self.params[0][4])  # Subsample ratio of the training instance. default = 1
        colsample_bytree = float(self.params[0][5])  # Subsample ratio of columns when constructing each tree. default = 1
        colsample_bylevel = float(self.params[0][6])  # Subsample ratio of columns for each split, in each level. default = 1
        gamma = float(self.params[0][7])  # Minimum loss reduction required to make a further partition on a leaf node of the tree. default=0

        model_seed = 100

        model = XGBRegressor(seed=model_seed,
                             n_estimators=n_estimators,
                             max_depth=max_depth,
                             learning_rate=learning_rate,
                             min_child_weight=min_child_weight,
                             subsample=subsample,
                             colsample_bytree=colsample_bytree,
                             colsample_bylevel=colsample_bylevel,
                             gamma=gamma)

        # Train the regressor
        model.fit(self.X_train, self.y_train)
        est_scaled = model.predict(self.X_test)
        scaled_today_price = self.X_test.iloc[-1, self.price_index]
        scaled_tomorrow_price = est_scaled[0]
        tomorrow_percentage = ((scaled_tomorrow_price - scaled_today_price) / abs(scaled_today_price)) * 100
        today_price = self.data.iloc[-1]['price']
        return {'today_price': today_price, 'tomorrow_price': today_price + (today_price*(tomorrow_percentage/100)), 'tomorrow_percentage': tomorrow_percentage}



