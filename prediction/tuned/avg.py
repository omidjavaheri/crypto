import numpy as np

from prediction.tuned.BaseModelForTuned import BaseModelForTuned


class Avg(BaseModelForTuned):

    def fit_and_predict(self):
        N = 2
        today_price = self.data.iloc[-1]['price']
        tomorrow_price = self.data.tail(N)['price'].mean()
        tomorrow_percentage = ((tomorrow_price - today_price) / abs(today_price)) * 100
        return {'today_price': today_price, 'tomorrow_price': tomorrow_price,
                'tomorrow_percentage': tomorrow_percentage}
