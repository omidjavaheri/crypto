from prediction.tuned.BaseModelForTuned import BaseModelForTuned
import pandas as pd
from prediction.Helper import targets, features
from sklearn.preprocessing import StandardScaler
from sklearn.ensemble import RandomForestRegressor

class Forest(BaseModelForTuned):

    def split_train_test(self):
        self.X_train = self.data[:-1][features]
        self.y_train = self.data[:-1][targets]
        self.X_test = (self.data.tail(1))[:][features]

    def fit_and_predict(self):
        pred_min = 0
        regr = RandomForestRegressor(random_state=0,
                                     n_estimators=200,
                                     min_samples_split=5,
                                     min_samples_leaf=1,
                                     max_features='sqrt',
                                     max_depth=35,
                                     bootstrap=True)

        regr.fit(self.X_train, self.y_train)

        pred = regr.predict(self.X_test)
        pred = pred[0]
        # If the values are < pred_min, set it to be pred_min
        if pred < pred_min:
            pred = pred_min

        today_price = self.data.iloc[-1]['price']
        tomorrow_price = pred
        tomorrow_percentage = ((tomorrow_price - today_price) / abs(today_price)) * 100
        return {'today_price': today_price, 'tomorrow_price': tomorrow_price,
                'tomorrow_percentage': tomorrow_percentage}



