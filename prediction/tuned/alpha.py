
from prediction.tuned.BaseModelForTuned import BaseModelForTuned
import joblib
from sklearn.preprocessing import MinMaxScaler
import os

class Alpha(BaseModelForTuned):

    def fit_and_predict(self):
        feat = ['price', 'volume', 'marketCap', 'twitterFollowers',
                'redditAveragePosts48h', 'redditAverageComments48h',
                'redditSubscribers', 'redditAccountsActive48h',
                'telegramChanelUserCount', 'alexaRank', 'bingMatches', 'forks', 'stars',
                'subscribers', 'totalIssues', 'closedIssues', 'pullRequestsMerged',
                'pullRequestContributors', 'additionsIn4Week', 'deletionIn4Week',
                'commitCount4Weeks']

        THIS_FOLDER = os.path.dirname(os.path.abspath(__file__))
        file = os.path.join(THIS_FOLDER, './alpha_model_v1_(7.8.2020).joblib')
        regr = joblib.load(file)

        min_max_scaler = MinMaxScaler()
        min_max_scaler.fit(self.data[['price']])

        test = self.data.tail(1)[feat]
        test['price'] = min_max_scaler.transform(test[['price']])

        pred = regr.predict(test)
        pred = pred[0]

        today_price = test['price'].values[0]
        tomorrow_price = pred
        tomorrow_percentage = ((tomorrow_price - today_price) / abs(today_price)) * 100
        return {'today_price': today_price, 'tomorrow_price': tomorrow_price,
                'tomorrow_percentage': tomorrow_percentage}

