from prediction.BaseModel import BaseModel
import numpy as np

from sklearn.linear_model import LinearRegression

class Regression(BaseModel):


    def fit_and_predict(self):
        """
        Given a dataframe, get prediction at timestep t using values from t-1, t-2, ..., t-N.
        Inputs
            df         : dataframe with the values you want to predict. Can be of any length.
            target_col : name of the column you want to predict e.g. 'adj_close'
            N          : get prediction at timestep t using values from t-1, t-2, ..., t-N
            pred_min   : all predictions should be >= pred_min
            offset     : for df we only do predictions for df[offset:]. e.g. offset can be size of training set
        Outputs
            pred_list  : the predictions for target_col. np.array of length len(df)-offset.
        """
        # Create linear regression object
        regr = LinearRegression(fit_intercept=True)

        pred_list = []
        N = 4
        offset = 4
        pred_min = 0
        for i in range(offset, len(self.data['price'])):
            X_train = np.array(range(len(self.data['price'][i - N:i])))  # e.g. [0 1 2 3 4]
            y_train = np.array(self.data['price_in_1_day'][i - N:i])  # e.g. [2944 3088 3226 3335 3436]
            X_train = X_train.reshape(-1, 1)  # e.g X_train =
            y_train = y_train.reshape(-1, 1)
            regr.fit(X_train, y_train)  # Train the model
            pred = regr.predict(np.array(N).reshape(1, -1))
            pred_list.append(pred[0][0])  # Predict the footfall using the model

        # If the values are < pred_min, set it to be pred_min
        pred_list = np.array(pred_list)
        pred_list[pred_list < pred_min] = pred_min

        today_price = self.data.iloc[-1]['price']
        tomorrow_price = pred_list[len(pred_list) - 1]
        tomorrow_percentage = ((tomorrow_price - today_price) / abs(today_price)) * 100
        return {'today_price': today_price, 'tomorrow_price': tomorrow_price, 'tomorrow_percentage': tomorrow_percentage}






