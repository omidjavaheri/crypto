import numpy as np

from prediction.BaseModel import BaseModel


class Avg(BaseModel):

    def fit_and_predict(self):
        """
        Given a dataframe, get prediction at timestep t using values from t-1, t-2, ..., t-N.
        Using simple moving average.
        Inputs
            df         : dataframe with the values you want to predict. Can be of any length.
            target_col : name of the column you want to predict e.g. 'adj_close'
            N          : get prediction at timestep t using values from t-1, t-2, ..., t-N
            pred_min   : all predictions should be >= pred_min
            offset     : for df we only do predictions for df[offset:]. e.g. offset can be size of training set
        Outputs
            pred_list  : list. The predictions for target_col. np.array of length len(df)-offset.
        """
        N = 2
        pred_min = 0
        offset = 2
        pred_list = self.data['price_in_1_day'].rolling(window=N, min_periods=1).mean()  # len(pred_list) = len(df)

        # Add one timestep to the predictions
        pred_list = np.concatenate((np.array([np.nan]), np.array(pred_list[:-1])))

        # If the values are < pred_min, set it to be pred_min
        pred_list = np.array(pred_list)
        pred_list[pred_list < pred_min] = pred_min

        today_price = self.data.iloc[-1]['price']
        tomorrow_price = pred_list[len(pred_list) - 1]
        tomorrow_percentage = ((tomorrow_price - today_price) / abs(today_price)) * 100
        return {'today_price': today_price, 'tomorrow_price': tomorrow_price, 'tomorrow_percentage': tomorrow_percentage}
