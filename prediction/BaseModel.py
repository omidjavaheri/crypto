
from prediction.Helper import prep_data

class BaseModel:
    def __init__(self, data, coin):
        self.data = data
        self.coin = coin
        self.price_index = 0

    def preprocess(self):
        cols = list(self.data)
        cols.insert(-1, cols.pop(cols.index('price')))
        self.data = self.data[cols]
        self.price_index = self.data.columns.get_loc('price')
        prep_data(self.data)

    def split_train_test(self):
        # print(2)
        pass

    def fit_and_predict(self):
        # print(2)
        pass

    def run(self):
        self.preprocess()
        self.split_train_test()
        return self.fit_and_predict()
