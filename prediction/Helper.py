import math
import warnings

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from sklearn.decomposition import PCA

warnings.filterwarnings("ignore")

zero_cleaner_features = ['marketCap', 'twitterFollowers', 'redditSubscribers', 'forks', 'stars',
                         'subscribers', 'totalIssues', 'closedIssues', 'pullRequestContributors', 'additionsIn4Week', 'deletionIn4Week',
                         'commitCount4Weeks']
features = ['price', 'volume', 'marketCap', 'twitterFollowers',
            'redditAveragePosts48h', 'redditAverageComments48h',
            'redditSubscribers', 'redditAccountsActive48h',
            'alexaRank', 'forks', 'stars',
            'subscribers', 'totalIssues', 'closedIssues',
            'pullRequestContributors', 'additionsIn4Week', 'deletionIn4Week',
            'commitCount4Weeks']
fill_nan = ['marketCap']
targets = ['price_in_1_day']


def zero_cleaner(column):
    up = 0
    if isinstance(column, pd.Series):
        column = column.values
    for i in range(len(column)):
        val = column[i]
        if column[i] == 0 and up == 0 and i >= 1:
            up = column[i - 1]
        if column[i] != 0 and up != 0:
            j = i
            ave = (column[i] + up) / 2
            while j >= 1 and column[j - 1] == 0:
                column[j - 1] = ave
                j -= 1
            up = 0
        # today fix
        if i == len(column)-1 and val==0:
            column[i] = up


def target_generator(data):
    b = data['price']
    if isinstance(b, pd.Series):
        b = b.values
    for in_days in range(1, 2):
        target = 'price_in_{}_day'.format(in_days)
        data[target] = data['price']
        a = data[target]
        if isinstance(a, pd.Series):
            a = a.values
        for i in range(len(b)):
            if i < len(b) - in_days:
                a[i] = b[i + in_days]
            else:
                a[i] = b[i - 5:i].mean()



def prep_data(data):
    for feature in fill_nan:
        data[feature] = data[feature].fillna(0)
    for feature in zero_cleaner_features:
        zero_cleaner(data[feature])
    target_generator(data)
    # grow_label_generator(data)

def load_data(path):
    return pd.read_csv(path)


def save_data(path, data):
    data.to_csv(path)


