from keras.models import Sequential
from keras.layers.core import Dense, Dropout
import numpy as np
import pandas as pd
from sklearn.preprocessing import StandardScaler

import tensorflow as tf
tf.compat.v1.disable_eager_execution()
keras = tf.keras
from tqdm import tqdm_notebook
from sklearn.metrics import mean_squared_error
import math
from tuner.models.BaseModel import BaseModel

class Rnn(BaseModel):

    def create_table_if_not_exist(self):
        create_table_query = '''CREATE TABLE rnn (
            id bigserial primary key,
            coin text NOT NULL,
            N text,
            units text,
            dropout text,
            optimizer text,
            epochs text,
            batch_size text
        ); '''
        con = None
        try:
            con = self.postgreSQL_pool.getconn()
            cur = con.cursor()
            cur.execute("select * from information_schema.tables where table_name=%s", ('rnn',))
            if bool(cur.rowcount) is False:
                cur.execute(create_table_query)
                con.commit()
        finally:
            self.postgreSQL_pool.putconn(con)

    def insert_row_for_coin_if_not_exist(self):
        con = None
        try:
            con = self.postgreSQL_pool.getconn()
            cur = con.cursor()
            cur.execute("select 1 from rnn where coin=%s", (self.coin,))
            if not bool(cur.rowcount):
                cur.execute("insert into rnn (coin, N, units, dropout, optimizer, epochs, batch_size) "
                            "values (%s, NULL, NULL, NULL, NULL, NULL, NULL)", (self.coin,))
                con.commit()
        finally:
            self.postgreSQL_pool.putconn(con)

    def params_not_exist_completely(self):
        con = None
        try:
            con = self.postgreSQL_pool.getconn()
            cur = con.cursor()
            cur.execute("select 1 from rnn where coin=%s and (N is NULL or units is NULL or dropout is NULL "
                        "or optimizer is NULL or epochs is NULL or batch_size is NULL )", (self.coin,))
            return bool(cur.rowcount)
        finally:
            self.postgreSQL_pool.putconn(con)

    def update_param_in_db(self, name, value):
        con = None
        try:
            con = self.postgreSQL_pool.getconn()
            cur = con.cursor()
            cur.execute("update rnn set {} = %s where coin=%s".format(name), (value, self.coin,))
            con.commit()
        finally:
            self.postgreSQL_pool.putconn(con)

    def standard_scaler(self, x_train, x_test):
        train_samples, train_nx, train_ny = x_train.shape
        test_samples, test_nx, test_ny = x_test.shape

        x_train = x_train.reshape((train_samples, train_nx * train_ny))
        x_test = x_test.reshape((test_samples, test_nx * test_ny))

        scaler = StandardScaler()
        x_train = scaler.fit_transform(x_train)
        x_test = scaler.transform(x_test)

        x_train = x_train.reshape((train_samples, train_nx, train_ny))
        x_test = x_test.reshape((test_samples, test_nx, test_ny))

        return x_train, x_test

    def prepare_train_test(self):
        amount_of_features = len(self.data.columns)
        data = self.data.values

        sequence_length = self.window + 1
        result = []
        for index in range(len(data) - sequence_length):
            result.append(data[index: index + sequence_length])

        result = np.array(result)
        row = round(0.9 * result.shape[0])
        train = result[: int(row), :]

        train, result = self.standard_scaler(train, result)

        self.x_train = train[:, : -1]
        self.y_train = train[:, -1][:, -1]
        self.x_test = result[int(row):, : -1]
        self.y_test = result[int(row):, -1][:, -1]

        self.x_train = np.reshape(self.x_train, (self.x_train.shape[0], self.x_train.shape[1], amount_of_features))
        self.x_test = np.reshape(self.x_test, (self.x_test.shape[0], self.x_test.shape[1], amount_of_features))


    def get_mape(self, y_true, y_pred):
        y_true, y_pred = np.array(y_true), np.array(y_pred)
        return np.mean(np.abs((y_true - y_pred) / y_true)) * 100

    def train_pred_eval_model(self):
        keras.backend.clear_session()
        tf.random.set_seed(42)
        np.random.seed(42)
        model = Sequential()
        model.add(keras.layers.SimpleRNN(units=self.lstm_units, return_sequences=True, input_shape=(self.x_train.shape[1], self.x_train.shape[2])))
        model.add(Dropout(self.dropout_prob))  # Add dropout with a probability of 0.5
        model.add(keras.layers.SimpleRNN(units=self.lstm_units))
        model.add(Dropout(self.dropout_prob))  # Add dropout with a probability of 0.5
        model.add(Dense(1))

        # Compile and fit the LSTM network
        model.compile(loss='mean_squared_error', optimizer=self.optimizer)
        model.fit(self.x_train, self.y_train, epochs=self.epochs, batch_size=self.batch_size, verbose=0)

        # Do prediction
        est = model.predict(self.x_test)

        # Calculate RMSE and MAPE
        #     print("x_cv_scaled = " + str(x_cv_scaled))
        #     print("est_scaled = " + str(est_scaled))
        #     print("est = " + str(est))
        rmse = math.sqrt(mean_squared_error(self.y_test, est))
        mape = self.get_mape(self.y_test, est)

        return rmse, mape, est

    def tune_N(self):
        param_label = 'N'
        param_list = range(3, 60)
        error_rate = {param_label: [], 'rmse': [], 'mape_pct': []}
        for param in tqdm_notebook(param_list):
            self.window = param
            self.prepare_train_test()
            rmse, mape, _ = self.train_pred_eval_model()
            error_rate[param_label].append(param)
            error_rate['rmse'].append(rmse)
            error_rate['mape_pct'].append(mape)

        error_rate = pd.DataFrame(error_rate)
        temp = error_rate[error_rate['rmse'] == error_rate['rmse'].min()]
        N_opt = temp['N'].values[0]
        self.update_param_in_db("N", str(N_opt))

    def tune_epochs_and_batch_size(self):
        param_label = 'epochs'
        param_list = [10, 20, 30, 40, 50, 60, 70, 80, 90]

        param2_label = 'batch_size'
        param2_list = [8, 16, 32, 64, 128]
        error_rate = {param_label: [],param2_label: [], 'rmse': [], 'mape_pct': []}
        for param in tqdm_notebook(param_list):
            for param2 in tqdm_notebook(param2_list):
                self.epochs = param
                self.batch_size = param2
                rmse, mape, _ = self.train_pred_eval_model()
                error_rate[param_label].append(param)
                error_rate[param2_label].append(param2)
                error_rate['rmse'].append(rmse)
                error_rate['mape_pct'].append(mape)
        error_rate = pd.DataFrame(error_rate)
        temp = error_rate[error_rate['rmse'] == error_rate['rmse'].min()]
        epochs_opt = temp[param_label].values[0]
        batch_size_opt = temp[param2_label].values[0]
        self.update_param_in_db("epochs", str(epochs_opt))
        self.update_param_in_db("batch_size", str(batch_size_opt))

    def tune_units_and_dropout(self):
        param_label = 'lstm_units'
        param_list = [10, 50, 64, 128]
        param2_label = 'dropout_prob'
        param2_list = [0.2, 0.3, 0.5, 0.6, 0.7, 0.8, 0.9]
        error_rate = {param_label: [], param2_label: [], 'rmse': [], 'mape_pct': []}
        for param in tqdm_notebook(param_list):
            for param2 in tqdm_notebook(param2_list):
                self.lstm_units = param
                self.dropout_prob = param2
                rmse, mape, _ = self.train_pred_eval_model()
                error_rate[param_label].append(param)
                error_rate[param2_label].append(param2)
                error_rate['rmse'].append(rmse)
                error_rate['mape_pct'].append(mape)

        error_rate = pd.DataFrame(error_rate)
        temp = error_rate[error_rate['rmse'] == error_rate['rmse'].min()]
        lstm_units_opt = temp[param_label].values[0]
        dropout_prob_opt = temp[param2_label].values[0]
        self.update_param_in_db("units", str(lstm_units_opt))
        self.update_param_in_db("dropout", str(dropout_prob_opt))

    def tune_optimizer(self):
        param_label = 'optimizer'
        param_list = ['adam', 'sgd', 'rmsprop', 'adagrad', 'adadelta', 'adamax', 'nadam']
        error_rate = {param_label: [], 'rmse': [], 'mape_pct': []}
        for param in tqdm_notebook(param_list):
            self.optimizer = param
            rmse, mape, _ = self.train_pred_eval_model()
            error_rate[param_label].append(param)
            error_rate['rmse'].append(rmse)
            error_rate['mape_pct'].append(mape)

        error_rate = pd.DataFrame(error_rate)
        temp = error_rate[error_rate['rmse'] == error_rate['rmse'].min()]
        optimizer_opt = temp[param_label].values[0]
        self.update_param_in_db("optimizer", str(optimizer_opt))

    def reset_params(self):
        self.window = 20
        self.lstm_units = 50
        self.dropout_prob = 0.5
        self.optimizer = 'adam'
        self.epochs = 1
        self.batch_size = 1
        
    def process(self):
        self.create_table_if_not_exist()
        self.insert_row_for_coin_if_not_exist()
        if self.params_not_exist_completely():
            self.reset_params()
            self.tune_N()
            self.reset_params()
            self.prepare_train_test()
            self.tune_epochs_and_batch_size()
            self.reset_params()
            self.tune_units_and_dropout()
            self.reset_params()
            self.tune_optimizer()
