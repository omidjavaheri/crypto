import numpy as np
import math

import itertools
from sklearn.linear_model import LinearRegression
from tuner.models.BaseModel import BaseModel
from prediction.Helper import features

class Regression(BaseModel):

    def create_table_if_not_exist(self):
        create_table_query = '''CREATE TABLE regression (
            id bigserial primary key,
            coin text NOT NULL,
            error_type text,
            N text
        ); '''
        con = None
        try:
            con = self.postgreSQL_pool.getconn()
            cur = con.cursor()
            cur.execute("select * from information_schema.tables where table_name=%s", ('regression',))
            if bool(cur.rowcount) is False:
                cur.execute(create_table_query)
                con.commit()
        finally:
            self.postgreSQL_pool.putconn(con)

    def insert_row_for_coin_if_not_exist(self, error_type):
        con = None
        try:
            con = self.postgreSQL_pool.getconn()
            cur = con.cursor()
            cur.execute("select 1 from regression where coin=%s and error_type=%s", (self.coin, error_type))
            if not bool(cur.rowcount):
                cur.execute("insert into regression (coin, error_type, N) "
                            "values (%s, %s, NULL)", (self.coin, error_type,))
                con.commit()
        finally:
            self.postgreSQL_pool.putconn(con)

    def params_not_exist_completely(self):
        con = None
        try:
            con = self.postgreSQL_pool.getconn()
            cur = con.cursor()
            cur.execute("select 1 from regression where coin=%s and (error_type='RMSE' or error_type='MAPE') and (N is NULL)", (self.coin,))
            return bool(cur.rowcount)
        finally:
            self.postgreSQL_pool.putconn(con)

    def update_param_in_db(self, name, value, error_type):
        con = None
        try:
            con = self.postgreSQL_pool.getconn()
            cur = con.cursor()
            cur.execute("update regression set {} = %s where coin=%s and error_type=%s".format(name), (value, self.coin, error_type))
            con.commit()
        finally:
            self.postgreSQL_pool.putconn(con)

    def fit_and_predict(self, data, **params):
        regr = LinearRegression(fit_intercept=True)
        params = params['params']['params']
        N = params[0]
        pred_min = 0
        X_train = data[data.shape[0] - N - 1:data.shape[0] - 1][features]
        y_train = data[data.shape[0] - N - 1:data.shape[0] - 1]['price_in_1_day']
        test = data.tail(1)[features]
        regr.fit(X_train, y_train)
        pred = regr.predict(test)
        pred = pred[0]
        if pred < pred_min:
            pred = pred_min
        today_price = data.iloc[-1]['price']
        tomorrow_price = pred
        tomorrow_percentage = ((tomorrow_price - today_price) / abs(today_price)) * 100
        return {'today_price': today_price, 'tomorrow_price': tomorrow_price,
                'tomorrow_percentage': tomorrow_percentage}


    def simulation(self, **params):
        predict_values = []
        for i in range(self.simulation_size, 0, -1):
            today_data = self.data[:self.data_size - i]
            tomorrow_predict = self.fit_and_predict(today_data, params=params)
            predict_values.append(tomorrow_predict['tomorrow_price'])
        return predict_values

    def get_mape(self, y_true, y_pred):
        y_true, y_pred = np.array(y_true), np.array(y_pred)
        return np.mean(np.abs((y_true - y_pred) / y_true)) * 100

    def get_rmse(self, y_true, y_pred):
        return math.sqrt(np.mean((np.array(y_true) - np.array(y_pred)) ** 2))

    def process(self):
        self.create_table_if_not_exist()
        self.insert_row_for_coin_if_not_exist("RMSE")
        self.insert_row_for_coin_if_not_exist("MAPE")
        if self.params_not_exist_completely():
            self.simulation_size = 60
            self.data_size = self.data.shape[0]
            self.true_values = self.data[self.data_size - self.simulation_size:]['price'].values
            N = range(2, 900)
            all_features_list = [N]
            comb = list(itertools.product(*all_features_list))
            rmse_result = []
            mape_result = []
            min_i_rmse = 0
            min_val_rmse = 10000
            min_i_mape = 0
            min_val_mape = 10000
            for i in comb:
                predict_values = None
                try:
                    predict_values = self.simulation(params=i)
                except:
                    print("N is bigger than num of records in regression")
                if predict_values is not None:
                    rmse = self.get_rmse(self.true_values, predict_values)
                    mape = self.get_mape(self.true_values, predict_values)
                    if min_val_mape > mape:
                        min_val_mape = mape
                        min_i_mape = i
                    if min_val_rmse > rmse:
                        min_val_rmse = rmse
                        min_i_rmse = i
                    rmse_result.append(rmse)
                    mape_result.append(mape)
            self.update_param_in_db("N", min_i_rmse, "RMSE")
            self.update_param_in_db("N", min_i_mape, "MAPE")

