
from prediction.Helper import prep_data
import psycopg2
from psycopg2 import pool


class BaseModel:
    def __init__(self, data, coin, pool):
        self.data = data
        self.coin = coin
        self.price_index = 0
        self.postgreSQL_pool = pool

    def preprocess(self):
        cols = list(self.data)
        cols.insert(-1, cols.pop(cols.index('price')))
        self.data = self.data[cols]
        self.price_index = self.data.columns.get_loc('price')
        prep_data(self.data)


    def process(self):
        pass

    def run(self):
        self.preprocess()
        self.process()

