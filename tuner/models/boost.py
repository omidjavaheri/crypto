import math
import numpy as np
import pandas as pd

from sklearn.metrics import mean_squared_error
from sklearn.preprocessing import StandardScaler
from tqdm import tqdm_notebook
from xgboost import XGBRegressor
from tuner.models.BaseModel import BaseModel
from prediction.Helper import targets, features

class Boost(BaseModel):

    def create_table_if_not_exist(self):
        create_table_query = '''CREATE TABLE boost (
            id bigserial primary key,
            coin text NOT NULL,
            error_type text,
            n_estimators text,
            max_depth text,
            learning_rate text,
            min_child_weight text,
            subsample text,
            colsample_bytree text,
            colsample_bylevel text,
            gamma text
        ); '''
        con = None
        try:
            con = self.postgreSQL_pool.getconn()
            cur = con.cursor()
            cur.execute("select * from information_schema.tables where table_name=%s", ('boost',))
            if bool(cur.rowcount) is False:
                cur.execute(create_table_query)
                con.commit()
        finally:
            self.postgreSQL_pool.putconn(con)

    def insert_row_for_coin_if_not_exist(self, error_type):
        con = None
        try:
            con = self.postgreSQL_pool.getconn()
            cur = con.cursor()
            cur.execute("select 1 from boost where coin=%s and error_type=%s", (self.coin, error_type))
            if not bool(cur.rowcount):
                cur.execute("insert into boost (coin, error_type, n_estimators, max_depth, learning_rate, min_child_weight, subsample, colsample_bytree, colsample_bylevel, gamma) "
                            "values (%s, %s, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)", (self.coin, error_type))
                con.commit()
        finally:
            self.postgreSQL_pool.putconn(con)

    def params_not_exist_completely(self):
        con = None
        try:
            con = self.postgreSQL_pool.getconn()
            cur = con.cursor()
            cur.execute("select 1 from boost where coin=%s and (error_type='RMSE' or error_type='MAPE') and "
                        "(n_estimators is NULL or max_depth is NULL or learning_rate is NULL or min_child_weight is NULL "
                        "or subsample is NULL or colsample_bytree is NULL or colsample_bylevel is NULL or gamma is NULL )", (self.coin,))
            return bool(cur.rowcount)
        finally:
            self.postgreSQL_pool.putconn(con)

    def update_param_in_db(self, name, value, error_type):
        con = None
        try:
            con = self.postgreSQL_pool.getconn()
            cur = con.cursor()
            cur.execute("update boost set {} = %s where coin=%s and error_type=%s".format(name), (value, self.coin, error_type))
            con.commit()
        finally:
            self.postgreSQL_pool.putconn(con)

    def reset_params(self):
        self.n_estimators = 100  # Number of boosted trees to fit. default = 100
        self.max_depth = 3  # Maximum tree depth for base learners. default = 3
        self.learning_rate = 0.1  # Boosting learning rate (xgb’s “eta”). default = 0.1
        self.min_child_weight = 1  # Minimum sum of instance weight(hessian) needed in a child. default = 1
        self.subsample = 1  # Subsample ratio of the training instance. default = 1
        self.colsample_bytree = 1  # Subsample ratio of columns when constructing each tree. default = 1
        self.colsample_bylevel = 1  # Subsample ratio of columns for each split, in each level. default = 1
        self.gamma = 0
        self.model_seed = 100

    def get_mape(self, y_true, y_pred):
        y_true, y_pred = np.array(y_true), np.array(y_pred)
        return np.mean(np.abs((y_true - y_pred) / y_true)) * 100

    def train_pred_eval_model(self):
        model = XGBRegressor(seed=self.model_seed,
                             n_estimators=self.n_estimators,
                             max_depth=self.max_depth,
                             learning_rate=self.learning_rate,
                             min_child_weight=self.min_child_weight,
                             subsample=self.subsample,
                             colsample_bytree=self.colsample_bytree,
                             colsample_bylevel=self.colsample_bylevel,
                             gamma=self.gamma)
        model.fit(self.x_train, self.y_train)
        est = model.predict(self.x_test)
        rmse = math.sqrt(mean_squared_error(self.y_test, est))
        mape = self.get_mape(self.y_test, est)
        return rmse, mape, est

    def prepare_train_test(self):
        scaler = StandardScaler()
        data_scaled = scaler.fit_transform(self.data[:])
        # Convert the numpy array back into pandas dataframe
        data_scaled = pd.DataFrame(data_scaled, columns=self.data.columns)
        num_test = int(0.2 * len(data_scaled))
        num_train = len(data_scaled) - num_test
        self.x_train = data_scaled[:num_train][features]
        self.price_index = self.x_train.columns.get_loc('price')
        self.y_train = data_scaled[:num_train][targets]
        self.x_test = data_scaled[num_train:][features]
        self.y_test = data_scaled[num_train:][targets]

    def tune_N_estimator_and_max_depth(self):
        param_label = 'n_estimators'
        param_list = range(10, 310, 10)
        param2_label = 'max_depth'
        param2_list = [2, 3, 4, 5, 6, 7, 8, 9]
        error_rate = {param_label: [], param2_label: [], 'rmse': [], 'mape_pct': []}
        for param in tqdm_notebook(param_list):
            for param2 in param2_list:
                self.n_estimators = param
                self.max_depth = param2
                rmse, mape, _ = self.train_pred_eval_model()
                error_rate[param_label].append(param)
                error_rate[param2_label].append(param2)
                error_rate['rmse'].append(rmse)
                error_rate['mape_pct'].append(mape)
        error_rate = pd.DataFrame(error_rate)
        temp = error_rate[error_rate['rmse'] == error_rate['rmse'].min()]
        n_estimators_opt = temp['n_estimators'].values[0]
        max_depth_opt = temp['max_depth'].values[0]
        self.update_param_in_db("n_estimators", str(n_estimators_opt), "RMSE")
        self.update_param_in_db("max_depth", str(max_depth_opt), "RMSE")
        temp = error_rate[error_rate['mape_pct'] == error_rate['mape_pct'].min()]
        self.update_param_in_db("n_estimators", str(temp['n_estimators'].values[0]), "MAPE")
        self.update_param_in_db("max_depth", str(temp['max_depth'].values[0]), "MAPE")

    def tune_learning_rate_and_min_child_weight(self):
        param_label = 'learning_rate'
        param_list = [0.001, 0.005, 0.01, 0.05, 0.1, 0.2, 0.3]
        param2_label = 'min_child_weight'
        param2_list = range(5, 21, 1)
        error_rate = {param_label: [], param2_label: [], 'rmse': [], 'mape_pct': []}
        for param in tqdm_notebook(param_list):
            for param2 in param2_list:
                self.learning_rate = param
                self.min_child_weight = param2
                rmse, mape, _ = self.train_pred_eval_model()
                error_rate[param_label].append(param)
                error_rate[param2_label].append(param2)
                error_rate['rmse'].append(rmse)
                error_rate['mape_pct'].append(mape)

        error_rate = pd.DataFrame(error_rate)
        temp = error_rate[error_rate['rmse'] == error_rate['rmse'].min()]
        learning_rate_opt = temp['learning_rate'].values[0]
        min_child_weight_opt = temp['min_child_weight'].values[0]
        self.update_param_in_db("learning_rate", str(learning_rate_opt), "RMSE")
        self.update_param_in_db("min_child_weight", str(min_child_weight_opt), "RMSE")
        temp = error_rate[error_rate['mape_pct'] == error_rate['mape_pct'].min()]
        self.update_param_in_db("learning_rate", str(temp['learning_rate'].values[0]), "MAPE")
        self.update_param_in_db("min_child_weight", str(temp['min_child_weight'].values[0]), "MAPE")

    def tune_subsample_and_gamma(self):
        param_label = 'subsample'
        param_list = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1]
        param2_label = 'gamma'
        param2_list = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1]
        error_rate = {param_label: [], param2_label: [], 'rmse': [], 'mape_pct': []}
        for param in tqdm_notebook(param_list):
            for param2 in param2_list:
                self.subsample = param
                self.gamma = param2
                rmse, mape, _ = self.train_pred_eval_model()
                error_rate[param_label].append(param)
                error_rate[param2_label].append(param2)
                error_rate['rmse'].append(rmse)
                error_rate['mape_pct'].append(mape)

        error_rate = pd.DataFrame(error_rate)
        temp = error_rate[error_rate['rmse'] == error_rate['rmse'].min()]
        subsample_opt = temp['subsample'].values[0]
        gamma_opt = temp['gamma'].values[0]
        self.update_param_in_db("subsample", str(subsample_opt), "RMSE")
        self.update_param_in_db("gamma", str(gamma_opt), "RMSE")
        temp = error_rate[error_rate['mape_pct'] == error_rate['mape_pct'].min()]
        self.update_param_in_db("subsample", str(temp['subsample'].values[0]), "MAPE")
        self.update_param_in_db("gamma", str(temp['gamma'].values[0]), "MAPE")

    def tune_bytree_and_bylevel(self):
        param_label = 'colsample_bytree'
        param_list = [0.5, 0.6, 0.7, 0.8, 0.9, 1]
        param2_label = 'colsample_bylevel'
        param2_list = [0.5, 0.6, 0.7, 0.8, 0.9, 1]
        error_rate = {param_label: [], param2_label: [], 'rmse': [], 'mape_pct': []}
        for param in tqdm_notebook(param_list):
            for param2 in param2_list:
                self.colsample_bytree = param
                self.colsample_bylevel = param2
                rmse, mape, _ = self.train_pred_eval_model()
                error_rate[param_label].append(param)
                error_rate[param2_label].append(param2)
                error_rate['rmse'].append(rmse)
                error_rate['mape_pct'].append(mape)

        error_rate = pd.DataFrame(error_rate)
        temp = error_rate[error_rate['rmse'] == error_rate['rmse'].min()]
        colsample_bytree_opt = temp['colsample_bytree'].values[0]
        colsample_bylevel_opt = temp['colsample_bylevel'].values[0]
        self.update_param_in_db("colsample_bytree", str(colsample_bytree_opt), "RMSE")
        self.update_param_in_db("colsample_bylevel", str(colsample_bylevel_opt), "RMSE")
        temp = error_rate[error_rate['mape_pct'] == error_rate['mape_pct'].min()]
        self.update_param_in_db("colsample_bytree", str(temp['colsample_bytree'].values[0]), "MAPE")
        self.update_param_in_db("colsample_bylevel", str(temp['colsample_bylevel'].values[0]), "MAPE")

    def process(self):
        self.create_table_if_not_exist()
        self.insert_row_for_coin_if_not_exist("RMSE")
        self.insert_row_for_coin_if_not_exist("MAPE")
        if self.params_not_exist_completely():
            self.prepare_train_test()
            self.reset_params()
            self.tune_N_estimator_and_max_depth()
            self.reset_params()
            self.tune_learning_rate_and_min_child_weight()
            self.reset_params()
            self.tune_subsample_and_gamma()
            self.reset_params()
            self.tune_bytree_and_bylevel()
