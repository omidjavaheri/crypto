import datetime
import os
import smtplib
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.utils import formatdate
from os.path import basename

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd


class GraphicalReport():

    def fetch_rows(self, con, date):
        cur = con.cursor()
        cur.execute("select * from pred where cast(tomorrow_date as varchar) like %s",
                    (date.strftime('%Y-%m-%d') + '%',))
        rows = cur.fetchall()
        return rows

    def graphical_report(self, today_date, con):
        yesterday = pd.DataFrame(self.fetch_rows(con, today_date - datetime.timedelta(days=1)))
        today = pd.DataFrame(self.fetch_rows(con, today_date))
        yesterday.columns = ['a', 'name', 'model', 'yesterday_price', 'predicted_price', 'percentage', 'date']
        today.columns = ['a', 'name', 'model', 'yesterday_price', 'predicted_price', 'percentage', 'date']
        result = yesterday.merge(today, on=['name', 'model'], how='inner', suffixes=('_1', '_2'))
        result = result[['name', 'model', 'date_1', 'yesterday_price_1', 'predicted_price_1', 'percentage_1', 'yesterday_price_2']]
        result.columns = ['name', 'model', 'date', 'yesterday_price', 'predicted_price', 'percentage', 'today_price']
        result['price_diff'] = result.today_price.astype(float) - result.yesterday_price.astype(float)
        result['price_diff'] = result['price_diff'] / abs(result['price_diff'])
        result['prediction'] = result.percentage.astype(float) / abs(result.percentage.astype(float))

        labels = []
        tp = []
        tn = []
        precision = []
        up = result[(result.model == result.model.unique()[0]) & (result.price_diff == 1)].shape[0]
        down = result[(result.model == result.model.unique()[0]) & (result.price_diff == -1)].shape[0]

        for m in result.model.unique():
            r = result[result.model == m]
            all_coins_count = r.shape[0]
            labels.append(m)
            ttp = 0
            ttn = 0
            tfp = 0

            for i, row in r.iterrows():
                if row['price_diff'] == -1 and row['prediction'] == -1:
                    ttn += 1
                elif row['price_diff'] == 1 and row['prediction'] == 1:
                    ttp += 1
                elif row['price_diff'] == -1 and row['prediction'] == 1:
                    tfp += 1
            precision.append(int(ttp / (ttp + tfp) * 100))
            tp.append(int(ttp / all_coins_count * 100))
            tn.append(int(ttn / all_coins_count * 100))
        #####################################################################################
        date = result.date.astype(str).values[0].split()[0]
        fig = plt.figure(figsize=(20, 20), dpi=80)
        fig.suptitle("Models and Market Report\ndate : {}".format(date), fontsize=24)

        # BarChart
        x = np.arange(len(labels))  # the label locations
        width = 0.35  # the width of the bars

        ax = plt.subplot(212)
        rects1 = ax.bar(x - width / 2, tp, width, label='True Positive')
        rects2 = ax.bar(x + width / 2, tn, width, label='True Negative')

        # Add some text for labels, title and custom x-axis tick labels, etc.
        ax.set_ylabel('Scores (%)')
        ax.set_title('Models Perfomances')
        ax.set_xticks(x)
        ax.set_xticklabels(labels)
        ax.legend()

        def autolabel(rects):
            """Attach a text label above each bar in *rects*, displaying its height."""
            for rect in rects:
                height = rect.get_height()
                ax.annotate('{}'.format(height),
                            xy=(rect.get_x() + rect.get_width() / 2, height),
                            xytext=(0, 3),  # 3 points vertical offset
                            textcoords="offset points",
                            ha='center', va='bottom')

        autolabel(rects1)
        autolabel(rects2)

        # yBarChart
        y_pos = np.arange(len(labels))
        ax2 = plt.subplot(221)
        plt.barh(y_pos, precision, align='center', alpha=1, color='lightskyblue')
        plt.yticks(y_pos, labels)
        for i, v in enumerate(precision):
            ax2.text(v, i, str(v))
        plt.xlabel('Score (%)')
        plt.title('Models Precisions')

        # pieChart
        labels = 'Up', 'Down'
        sizes = [up, down]
        colors = ['yellowgreen', 'lightcoral']
        explode = (0.1, 0)  # explode 1st slice
        plt.subplot(222)
        plt.pie(sizes, explode=explode, labels=labels, colors=colors, autopct='%1.1f%%', shadow=True, startangle=140)
        plt.axis('equal')
        plt.title('Market View\nNumber of Coins:{}, Up:{}, Down:{}'.format(down + up, up, down))

        file_path = '../prediction/data/report.png'
        THIS_FOLDER = os.path.dirname(os.path.abspath(__file__))
        file_path = os.path.join(THIS_FOLDER, file_path)
        if os.path.exists(file_path):
            os.remove(file_path)
        plt.savefig(file_path)
        self.send_mail("coindooni@gmail.com",
                       ["verdino1102@gmail.com"],
                       today_date.strftime('%Y-%m-%d') + ' Graphical Report', "no body",
                       [file_path])

    def send_mail(self, send_from, send_to, subject, text, files=None):
        assert isinstance(send_to, list)

        msg = MIMEMultipart()
        msg['From'] = send_from
        msg['To'] = ', '.join(send_to)
        msg['Date'] = formatdate(localtime=True)
        msg['Subject'] = subject

        msg.attach(MIMEText(text))

        for f in files or []:
            with open(f, "rb") as fil:
                part = MIMEApplication(
                    fil.read(),
                    Name=basename(f)
                )
            # After the file is closed
            part['Content-Disposition'] = 'attachment; filename="%s"' % basename(f)
            msg.attach(part)

        smtp = smtplib.SMTP('smtp.gmail.com', 587)
        smtp.starttls()
        smtp.login("coindooni@gmail.com", "Pelak135@Vahede2")
        smtp.sendmail(send_from, send_to, msg.as_string())
        smtp.close()

#     plt.show()
