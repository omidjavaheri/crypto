from keras.models import Sequential
from keras.layers.core import Dense, Dropout, Activation
from keras.layers.recurrent import LSTM
import numpy as np
import pandas as pd
import sklearn.preprocessing as prep

import tensorflow as tf
keras = tf.keras
import matplotlib.pyplot as plt

zero_cleaner_features =['twitterFollowers','redditSubscribers','forks', 'stars',
       'subscribers', 'totalIssues', 'closedIssues','pullRequestContributors', 'additionsIn4Week', 'deletionIn4Week',
       'commitCount4Weeks']
features =['price', 'volume', 'marketCap', 'twitterFollowers',
       'redditAveragePosts48h', 'redditAverageComments48h',
       'redditSubscribers', 'redditAccountsActive48h',
        'alexaRank', 'forks', 'stars',
       'subscribers', 'totalIssues', 'closedIssues',
       'pullRequestContributors', 'additionsIn4Week', 'deletionIn4Week',
       'commitCount4Weeks']
targets = ['price_in_1_day', 'price_in_2_day',
       'price_in_3_day', 'price_in_4_day', 'price_in_5_day', 'price_in_6_day',
       'price_in_7_day', 'price_in_8_day', 'price_in_9_day', 'price_in_10_day',
       'grow_in_1_day', 'grow_in_2_day', 'grow_in_3_day', 'grow_in_4_day',
       'grow_in_5_day', 'grow_in_6_day', 'grow_in_7_day', 'grow_in_8_day',
       'grow_in_9_day', 'grow_in_10_day']


def zero_cleaner(column):
    up =0
    for i in range(len(column)):
        val = column[i]
        if column[i] == 0 and up ==0 and i >=1:
            up = column[i-1]
        if column[i] != 0 and up!=0:
            j=i
            ave = (column[i]+up)/2
            while j>=1 and column[j-1] ==0:
                column[j-1] = ave
                j-=1
            up = 0

def target_generator(data):
    b = data['price']
    for in_days in range(1,2):
        target = 'price_in_{}_day'.format(in_days)
        data[target] = data['price']
        a = data[target]
        for i in range(len(b)):
            if i<len(b)-in_days:
                a[i] = b[i+in_days]
            else:
                a[i] = b[i-5:i].mean()

def grow_label_generator(data):
    b = data['price']
    for in_days in range(1,11):
        target = 'grow_in_{}_day'.format(in_days)
        data[target] = data['price']
        a = data[target]
        for i in range(len(b)):
            if i<len(b)-in_days:
                if a[i] < b[i+in_days]:
                    a[i] = 1
                else:
                    a[i] = 0
            else:
                a[i]=-1

def standard_scaler(X_train, X_test):
    train_samples, train_nx, train_ny = X_train.shape
    test_samples, test_nx, test_ny = X_test.shape

    X_train = X_train.reshape((train_samples, train_nx * train_ny))
    X_test = X_test.reshape((test_samples, test_nx * test_ny))

    preprocessor = prep.StandardScaler().fit(X_train)
    X_train = preprocessor.transform(X_train)
    X_test = preprocessor.transform(X_test)

    X_train = X_train.reshape((train_samples, train_nx, train_ny))
    X_test = X_test.reshape((test_samples, test_nx, test_ny))

    return X_train, X_test


def preprocess_data(stock, seq_len):
    amount_of_features = len(stock.columns)
    data = stock.values

    sequence_length = seq_len + 1
    result = []
    for index in range(len(data) - sequence_length):
        result.append(data[index: index + sequence_length])

    result = np.array(result)
    row = round(0.9 * result.shape[0])
    train = result[: int(row), :]

    train, result = standard_scaler(train, result)

    X_train = train[:, : -1]
    y_train = train[:, -1][:, -1]
    X_test = result[int(row):, : -1]
    y_test = result[int(row):, -1][:, -1]

    X_train = np.reshape(X_train, (X_train.shape[0], X_train.shape[1], amount_of_features))
    X_test = np.reshape(X_test, (X_test.shape[0], X_test.shape[1], amount_of_features))

    return [X_train, y_train, X_test, y_test]


def build_model(X, Y, Xtest, Ytest):
    keras.backend.clear_session()
    tf.random.set_seed(42)
    np.random.seed(42)

    model = Sequential()
    model.add(keras.layers.SimpleRNN(100, input_shape=(X.shape[1], X.shape[2]), return_sequences=True))
    model.add(Dropout(0.2))

    # second layer
    model.add(keras.layers.SimpleRNN(100))
    model.add(Dropout(0.2))

    model.add(Dense(1))

    optimizer = keras.optimizers.RMSprop(lr=0.001, rho=0.9, epsilon=1e-8, decay=0.0)
    model.compile(loss="mean_squared_error",
                  optimizer=optimizer, metrics=["mae"])
    reset_states = ResetStatesCallback()
    model_checkpoint = keras.callbacks.ModelCheckpoint(
        "my_checkpoint.h5", save_best_only=True)
    early_stopping = keras.callbacks.EarlyStopping(patience=50)
    model.fit(X, Y, epochs=500, batch_size=50, verbose=0,
              validation_data=(Xtest, Ytest),
              callbacks=[early_stopping, model_checkpoint, reset_states])
    return model


class ResetStatesCallback(keras.callbacks.Callback):
    def on_epoch_begin(self, epoch, logs):
        self.model.reset_states()

if __name__ == '__main__':
    df = pd.read_csv('prediction/data/day.csv')
    window = 20
    for feature in zero_cleaner_features:
        zero_cleaner(df[feature])
    target_generator(df)
    X_train, y_train, X_test, y_test = preprocess_data(df[:: -1], window)
    # print("X_train", X_train.shape)
    # print("y_train", y_train.shape)
    # print("X_test", X_test.shape)
    # print("y_test", y_test.shape)
    build_model(X_train, y_train, X_test, y_test)
    model = keras.models.load_model("my_checkpoint.h5")

    # plot result
    diff = []
    ratio = []
    pred = model.predict(X_test)
    for u in range(len(y_test)):
        pr = pred[u][0]
        ratio.append((y_test[u] / pr) - 1)
        diff.append(abs(y_test[u] - pr))
    plt.plot(pred, color='red', label='Prediction')
    plt.plot(y_test, color='blue', label='Ground Truth')
    plt.legend(loc='upper left')
    plt.show()