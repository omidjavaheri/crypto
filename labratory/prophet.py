import math
import matplotlib
import multiprocessing
import numpy as np
import pandas as pd
import pickle
import seaborn as sns
import time

import warnings

warnings.simplefilter(action='ignore', category=FutureWarning)

from datetime import date, datetime, timedelta
from fbprophet import Prophet
from joblib import Parallel, delayed
from matplotlib import pyplot as plt
from pylab import rcParams
from sklearn.metrics import mean_squared_error
from tqdm import tqdm_notebook

#### Input params ##################
stk_path = "./withDate.csv"
H = 21  # Forecast horizon
train_size = 252 * 1  # Use 1 years of data as train set. Note there are about 252 trading days in a year
val_size = 252  # Use 1 year of data as validation set
changepoint_prior_scale_list = [0.05, 0.5, 1, 1.5, 2.5]  # for hyperparameter tuning
fourier_order_list = [None, 2, 4, 6, 8, 10]  # for hyperparameter tuning
window_list = [None, 0, 1, 2]  # for hyperparameter tuning

fontsize = 14
ticklabelsize = 14
####################################

train_val_size = train_size + val_size  # Size of train+validation set


def get_mape(y_true, y_pred):
    """
    Compute mean absolute percentage error (MAPE)
    """
    y_true, y_pred = np.array(y_true), np.array(y_pred)
    return np.mean(np.abs((y_true - y_pred) / y_true)) * 100


def get_mae(a, b):
    """
    Comp mean absolute error e_t = E[|a_t - b_t|]. a and b can be lists.
    Returns a vector of len = len(a) = len(b)
    """
    return np.mean(abs(np.array(a) - np.array(b)))


def get_rmse(a, b):
    """
    Comp RMSE. a and b can be lists.
    Returns a scalar.
    """
    return math.sqrt(np.mean((np.array(a) - np.array(b)) ** 2))


def get_preds_prophet(df, H, changepoint_prior_scale=0.05, fourier_order=None, holidays=None):
    """
    Use Prophet to forecast for the next H timesteps, starting at df[len(df)]
    Inputs
        df: dataframe with headers 'ds' and 'y' (necessary for Prophet)
        H : forecast horizon
        changepoint_prior_scale : to detect changepoints in time series analysis trajectories
        fourier_order           : determines how quickly seasonality can change
        holidays                : dataframe containing holidays you will like to model.
                                  Must have 'holiday' and 'ds' columns
    Outputs
        A list of predictions
    """
    # Fit prophet model
    if holidays is not None:
        m = Prophet(changepoint_prior_scale=changepoint_prior_scale, holidays=holidays)
    else:
        m = Prophet(changepoint_prior_scale=changepoint_prior_scale)
    if (fourier_order is not None) and (~np.isnan(fourier_order)):  # add monthly seasonality
        m.add_seasonality(name='monthly', period=21, fourier_order=int(fourier_order))

    m.fit(df)

    # Make future dataframe
    future = m.make_future_dataframe(periods=2 * H)

    # Predict
    forecast = m.predict(future)  # Note this prediction includes the original dates

    return forecast['yhat'][len(df):len(df) + H]


def get_error_metrics(df, H, train_size, val_size, changepoint_prior_scale=0.05, fourier_order=None, holidays=None):
    """
    Given a dataframe consisting of both train+validation, do predictions of forecast horizon H on the validation set,
    at H/2 intervals.
    Inputs
        df                     : dataframe with headers 'ds' and 'y' (necessary for Prophet)
        H                      : forecast horizon
        train_size             : length of training set
        val_size               : length of validation set. Note len(df) = train_size + val_size
        changepoint_prior_scale: to detect changepoints in time series analysis trajectories
        fourier_order          : determines how quickly seasonality can change
        holidays               : dataframe containing holidays you will like to model.
                                 Must have 'holiday' and 'ds' columns
    Outputs
        mean of rmse, mean of mape, mean of mae, dict of predictions
    """
    assert len(df) == train_size + val_size

    # Predict using Prophet, and compute error metrics also
    rmse = []  # root mean square error
    mape = []  # mean absolute percentage error
    mae = []  # mean absolute error
    preds_dict = {}

    rmse_mean, mape_mean, mae_mean = get_preds_prophet_parallelized(df, H, changepoint_prior_scale, fourier_order,
                                                                    holidays)

    return rmse_mean, mape_mean, mae_mean


def hyperparam_tune_cp(df, H, train_size, val_size, changepoint_prior_scale_list):
    """
    Hyperparameter tuning - changepoint
    Inputs
        df                     : dataframe with headers 'ds' and 'y' (necessary for Prophet)
        H                      : forecast horizon
        train_size             : length of training set
        val_size               : length of validation set. Note len(df) = train_size + val_size
        changepoint_prior_scale_list: list of changepoint_prior_scale values to try
    Outputs
        optimum hyperparameters
    """
    rmse_mean_list = []
    mape_mean_list = []
    mae_mean_list = []
    for changepoint_prior_scale in tqdm_notebook(changepoint_prior_scale_list):
        print("changepoint_prior_scale = " + str(changepoint_prior_scale))
        rmse_mean, mape_mean, mae_mean = get_error_metrics(df, H, train_size, val_size, changepoint_prior_scale)
        rmse_mean_list.append(rmse_mean)
        mape_mean_list.append(mape_mean)
        mae_mean_list.append(mae_mean)

    # Create results dataframe
    results = pd.DataFrame({'changepoint_prior_scale': changepoint_prior_scale_list,
                            'rmse': rmse_mean_list,
                            'mape(%)': mape_mean_list,
                            'mae': mae_mean_list})

    # Return hyperparam corresponding to lowest error metric
    return changepoint_prior_scale_list[np.argmin(rmse_mean_list)], results


def hyperparam_tune_fo(df, H, train_size, val_size, fourier_order_list):
    """
    Hyperparameter tuning - fourier order
    Inputs
        df                     : dataframe with headers 'ds' and 'y' (necessary for Prophet)
        H                      : forecast horizon
        train_size             : length of training set
        val_size               : length of validation set. Note len(df) = train_size + val_size
        fourier_order_list     : list of fourier_order values to try
    Outputs
        optimum hyperparameters
    """
    rmse_mean_list = []
    mape_mean_list = []
    mae_mean_list = []
    for fourier_order in tqdm_notebook(fourier_order_list):
        print("fourier_order = " + str(fourier_order))
        rmse_mean, mape_mean, mae_mean = get_error_metrics(df, H, train_size, val_size, 0.05, fourier_order)
        rmse_mean_list.append(rmse_mean)
        mape_mean_list.append(mape_mean)
        mae_mean_list.append(mae_mean)

    # Create results dataframe
    results = pd.DataFrame({'fourier_order': fourier_order_list,
                            'rmse': rmse_mean_list,
                            'mape(%)': mape_mean_list,
                            'mae': mae_mean_list})

    # Return hyperparam corresponding to lowest error metric
    return fourier_order_list[np.argmin(rmse_mean_list)], results


def hyperparam_tune_wd(df, H, train_size, val_size, window_list, holidays):
    """
    Hyperparameter tuning - upper and lower windows for holidays
    Inputs
        df                     : dataframe with headers 'ds' and 'y' (necessary for Prophet)
        H                      : forecast horizon
        train_size             : length of training set
        val_size               : length of validation set. Note len(df) = train_size + val_size
        window_list            : list of upper and lower window values to try
        holidays               : dataframe containing holidays you will like to model.
                                 Must have 'holiday' and 'ds' columns
    Outputs
        optimum hyperparameters
    """
    rmse_mean_list = []
    mape_mean_list = []
    mae_mean_list = []
    for window in tqdm_notebook(window_list):
        print("window = " + str(window))

        if window is None:
            rmse_mean, mape_mean, mae_mean = get_error_metrics(df=df,
                                                               H=H,
                                                               train_size=train_size,
                                                               val_size=val_size,
                                                               holidays=None)
        else:
            # Add lower_window and upper_window which extend the holiday out to
            # [lower_window, upper_window] days around the date
            holidays['lower_window'] = -window
            holidays['upper_window'] = +window

            rmse_mean, mape_mean, mae_mean = get_error_metrics(df=df,
                                                               H=H,
                                                               train_size=train_size,
                                                               val_size=val_size,
                                                               holidays=holidays)
        rmse_mean_list.append(rmse_mean)
        mape_mean_list.append(mape_mean)
        mae_mean_list.append(mae_mean)

    # Create results dataframe
    results = pd.DataFrame({'window': window_list,
                            'rmse': rmse_mean_list,
                            'mape(%)': mape_mean_list,
                            'mae': mae_mean_list})

    # Return hyperparam corresponding to lowest error metric
    return window_list[np.argmin(rmse_mean_list)], results


def hyperparam_tune_cp_fo_wd(df, H, train_size, val_size, changepoint_prior_scale_list,
                             fourier_order_list, window_list, holidays):
    """
    Hyperparameter tuning - changepoint, fourier_order, holidays
    Inputs
        df                     : dataframe with headers 'ds' and 'y' (necessary for Prophet)
        H                      : forecast horizon
        train_size             : length of training set
        val_size               : length of validation set. Note len(df) = train_size + val_size
        changepoint_prior_scale_list: list of changepoint_prior_scale values to try
        fourier_order_list          : list of fourier_order values to try
        window_list                 : list of upper and lower window values to try
        holidays                    : dataframe containing holidays you will like to model.
                                      Must have 'holiday' and 'ds' columns
    Outputs
        optimum hyperparameters
    """
    rmse_mean_list = []
    mape_mean_list = []
    mae_mean_list = []
    cp_list = []
    fo_list = []
    wd_list = []
    for changepoint_prior_scale in tqdm_notebook(changepoint_prior_scale_list):
        for fourier_order in tqdm_notebook(fourier_order_list):
            for window in tqdm_notebook(window_list):

                if window is None:
                    rmse_mean, mape_mean, mae_mean = get_error_metrics(df,
                                                                       H,
                                                                       train_size,
                                                                       val_size,
                                                                       changepoint_prior_scale,
                                                                       fourier_order,
                                                                       holidays=None)
                else:
                    # Add lower_window and upper_window which extend the holiday out to
                    # [lower_window, upper_window] days around the date
                    holidays['lower_window'] = -window
                    holidays['upper_window'] = +window

                    rmse_mean, mape_mean, mae_mean = get_error_metrics(df,
                                                                       H,
                                                                       train_size,
                                                                       val_size,
                                                                       changepoint_prior_scale,
                                                                       fourier_order,
                                                                       holidays)
                rmse_mean_list.append(rmse_mean)
                mape_mean_list.append(mape_mean)
                mae_mean_list.append(mae_mean)
                cp_list.append(changepoint_prior_scale)
                fo_list.append(fourier_order)
                wd_list.append(window)

    # Return hyperparam corresponding to lowest error metric
    results = pd.DataFrame({'changepoint_prior_scale': cp_list,
                            'fourier_order': fo_list,
                            'window': wd_list,
                            'rmse': rmse_mean_list,
                            'mape(%)': mape_mean_list,
                            'mae': mae_mean_list})
    temp = results[results['rmse'] == results['rmse'].min()]
    changepoint_prior_scale_opt = temp['changepoint_prior_scale'].values[0]
    fourier_order_opt = temp['fourier_order'].values[0]
    window_opt = temp['window'].values[0]

    return changepoint_prior_scale_opt, fourier_order_opt, window_opt, results


def processInput(i, df, H, changepoint_prior_scale, fourier_order, holidays):
    preds_list = get_preds_prophet(df[i - train_size:i], H, changepoint_prior_scale, fourier_order, holidays)

    # Compute error metrics
    rmse = get_rmse(df[i:i + H]['y'], preds_list)
    mape = get_mape(df[i:i + H]['y'], preds_list)
    mae = get_mae(df[i:i + H]['y'], preds_list)

    return (rmse, mape, mae)


def get_preds_prophet_parallelized(df, H, changepoint_prior_scale=0.05, fourier_order=None, holidays=None):
    """
    This is a parallelized implementation of get_preds_prophet.
    Given a dataframe consisting of both train+validation, do predictions of forecast horizon H on the validation set,
    at H/2 intervals.
    Inputs
        df                     : dataframe with headers 'ds' and 'y' (necessary for Prophet)
        H                      : forecast horizon
        train_size             : length of training set
        val_size               : length of validation set. Note len(df) = train_size + val_size
        changepoint_prior_scale: to detect changepoints in time series analysis trajectories
        fourier_order          : determines how quickly seasonality can change
        holidays               : dataframe containing holidays you will like to model.
                                 Must have 'holiday' and 'ds' columns
    Outputs
        mean of rmse, mean of mape, mean of mae, dict of predictions
    """
    inputs = range(train_size, len(df) - H, int(H / 2))

    num_cores = multiprocessing.cpu_count()

    results = Parallel(n_jobs=num_cores)(
        delayed(processInput)(i, df, H, changepoint_prior_scale, fourier_order, holidays) for i in inputs)
    # results has format [(rmse1, mape1, mae1), (rmse2, mape2, mae2), ...]

    rmse = [errors[0] for errors in results]
    mape = [errors[1] for errors in results]
    mae = [errors[2] for errors in results]

    return np.mean(rmse), np.mean(mape), np.mean(mae)


def target_generator(data):
    b = data['price']
    for in_days in range(1, 2):
        target = 'price_in_{}_day'.format(in_days)
        data[target] = data['price']
        a = data[target]
        for i in range(len(b)):
            if i < len(b) - in_days:
                a[i] = b[i + in_days]
            else:
                a[i] = b[i - 5:i].mean()


df = pd.read_csv(stk_path, sep=",")
# Convert Date column to datetime
df.loc[:, 'date'] = pd.to_datetime(df['date'], format='%Y-%m-%d')
# Sort by datetime
df.sort_values(by='date', inplace=True, ascending=True)
target_generator(df)

df_prophet = df[['date', 'price']].rename(columns={'date': 'ds', 'price': 'y'})

i = train_val_size

# tic = time.time()
# fourier_order_opt, results = hyperparam_tune_fo(df_prophet[i-train_val_size:i],
#                                                 H,
#                                                 train_size,
#                                                 val_size,
#                                                 fourier_order_list)
# toc = time.time()
# print("Time taken = " + str((toc-tic)/60.0) + " mins")
#
# print("fourier_order_opt = " + str(fourier_order_opt))
rmse = []  # root mean square error
mape = []  # mean absolute percentage error
mae = []  # mean absolute error
preds_dict = {}
H = 5
changepoint_prior_scale_opt = 0.05
fourier_order_opt = None
i_list = list(range(train_val_size, len(df_prophet) - 21, 42))
for i in i_list:
    # for i in tqdm_notebook(range(train_val_size, len(df)-H, int(H/2))): # Do a forecast on day i
    print("Predicting on day %d, date %s" % (i, df_prophet['ds'][i]))

    # Get predictions using tuned hyperparams
    preds_list = get_preds_prophet(df_prophet[i - train_val_size:i],
                                   H,
                                   changepoint_prior_scale_opt,
                                   fourier_order_opt,
                                   holidays=None)
    # Collect the predictions
    preds_dict[i] = preds_list
    # Compute error metrics
    rmse.append(get_rmse(df_prophet[i:i + H]['y'], preds_list))
    mape.append(get_mape(df_prophet[i:i + H]['y'], preds_list))
    mae.append(get_mae(df_prophet[i:i + H]['y'], preds_list))
print("For forecast horizon %d, the mean RMSE is %f" % (H, np.mean(rmse)))
print("For forecast horizon %d, the mean MAPE is %f" % (H, np.mean(mape)))
print("For forecast horizon %d, the mean MAE is %f" % (H, np.mean(mae)))
rcParams['figure.figsize'] = 10, 8  # width 10, height 8
ax = df.plot(x='date', y='price', style='b-', grid=True)
# Plot the predictions
for key in preds_dict:
    ax.plot(df['date'][key:key + H], preds_dict[key])
ax.set_xlabel("date")
ax.set_ylabel("USD")
plt.show()
