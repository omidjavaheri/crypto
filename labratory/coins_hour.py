import numpy as np
import math
import matplotlib.pyplot as plt
import pandas as pd
from sklearn.preprocessing import MinMaxScaler, StandardScaler
plt.style.use("ggplot")

from keras.models import Sequential
from keras.layers import LSTM, Dense, Dropout
from keras.models import Sequential
from keras import layers

import tensorflow as tf
keras = tf.keras

import warnings
warnings.filterwarnings("ignore")
pd.set_option('display.max_columns', 50)
pd.set_option('display.max_rows', 100)

from sklearn.discriminant_analysis import LinearDiscriminantAnalysis as LDA
from sklearn.discriminant_analysis import QuadraticDiscriminantAnalysis as QDA
from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import StandardScaler
from sklearn.svm import LinearSVC, SVC
from sklearn.pipeline import make_pipeline
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import BaggingClassifier
from sklearn.ensemble import RandomForestClassifier, AdaBoostClassifier, GradientBoostingClassifier

from sklearn.model_selection import train_test_split,KFold, cross_val_score
from sklearn.metrics import confusion_matrix, classification_report, accuracy_score
from sklearn.preprocessing import LabelEncoder
import seaborn as sn
import matplotlib.pyplot as plt
import warnings
warnings.filterwarnings("ignore")
from mlxtend.plotting import plot_confusion_matrix
def make_report(y_pred , y_true):
    print ("")
    print ("Classification Report: ")
    print (classification_report(y_true, y_pred))
    cm = confusion_matrix(y_true, y_pred)
    print(cm)
    fig, ax = plot_confusion_matrix(conf_mat=cm)
    
def imp_df(column_names, importances):
    df = pd.DataFrame({'feature': column_names,
                       'feature_importance': importances}) \
           .sort_values('feature_importance', ascending = False) \
           .reset_index(drop = True)
    return df
    
from sklearn import preprocessing



data = pd.read_csv('datasets/coins project/hour.csv')
data['class'] = data['price'].diff(-1)*-1
data['class'] = (data['class'] / abs(data['class']) +1)/2
    
data['number_of_coins'] = data['marketCap'] / data['price']
data['number_of_coins_transactions'] = data['volume'] / data['price']

#Diff Features
for i in range(1,12+1): 
    data['d_price_{}'.format(str(i))] = data['price'].diff(i)
    data['d_marketCap_{}'.format(str(i))] = data['marketCap'].diff(i)
    data['d_volume_{}'.format(str(i))] = data['volume'].diff(i)
    data['d_noct_{}'.format(str(i))] = data['number_of_coins_transactions'].diff(i)
    data['d_noc_{}'.format(str(i))] = data['number_of_coins'].diff(i)
    
    data['a_noct_{}'.format(str(i))] = data['d_noct_{}'.format(str(i))] / abs(data['d_noct_{}'.format(str(i))])
    data['a_noc_{}'.format(str(i))] = data['d_noc_{}'.format(str(i))] / abs(data['d_noc_{}'.format(str(i))])
    data['a_price_{}'.format(str(i))] = data['d_price_{}'.format(str(i))] / abs(data['d_price_{}'.format(str(i))])
    data['a_volume_{}'.format(str(i))] = data['d_volume_{}'.format(str(i))] / abs(data['d_volume_{}'.format(str(i))])
    data['a_marketCap_{}'.format(str(i))] = data['d_marketCap_{}'.format(str(i))] / abs(data['d_marketCap_{}'.format(str(i))] )
    
    data['m_marketCap_d_noc_{}'.format(str(i))] = data['d_marketCap_{}'.format(str(i))]*data['d_noc_{}'.format(str(i))] 
    data['m_marketCap_d_noc_{}'.format(str(i))] = data['m_marketCap_d_noc_{}'.format(str(i))] / abs(data['m_marketCap_d_noc_{}'.format(str(i))])
    data['m_volume_d_noct_{}'.format(str(i))] = data['d_volume_{}'.format(str(i))]*data['d_noct_{}'.format(str(i))] 
    data['m_volume_d_noct_{}'.format(str(i))] = data['m_volume_d_noct_{}'.format(str(i))] / abs(data['m_volume_d_noct_{}'.format(str(i))])

    
# Date Features
data['date_parsed'] = pd.to_datetime(data['date'], format="%Y-%m-%d")
data['date'] = data['date_parsed'].dt.date
data['date'] = pd.to_datetime(data['date'], format="%Y-%m-%d")
data['hour'] = data['date_parsed'].dt.hour
pd.to_datetime(data['date'], format="%Y-%m-%d")
data['day'] = data['date_parsed'].dt.day
data['month'] = data['date_parsed'].dt.month
# data['year'] = data['date_parsed'].dt.year
data['week'] = data['date_parsed'].dt.week
# data['weekofyear'] = data['date_parsed'].dt.weekofyear
data['dayofweek'] = data['date_parsed'].dt.dayofweek
data['dayofyear'] = data['date_parsed'].dt.dayofyear
data['quarter'] = data['date_parsed'].dt.quarter
# data['is_month_start'] = data['date_parsed'].dt.is_month_start.astype(int)
# data['is_month_end'] = data['date_parsed'].dt.is_month_end.astype(int)
data = data.drop('date_parsed', axis =1)

# cleaning
data = data[~data.name.isin( ['handshake (hns)', 'handsha'] )]
data = data[data.date != '2020-11-01']
data = data.dropna()


bit = data[data.name == 'bitcoin (btc)']
nonbit = data[data.name != 'bitcoin (btc)']
data = pd.merge(nonbit, bit,  how='left', left_on=['date','hour'], right_on = ['date','hour'], suffixes=('', '_bitcoin'))
data = data.dropna()

le = preprocessing.LabelEncoder()
le.fit(data.name.values)
data['name'] = le.transform(data.name.values)

print(bit.shape, nonbit.shape,bit.shape[0]+nonbit.shape[0], data.shape, bit.shape[0]+nonbit.shape[0]-data.shape[0])


X = data.drop(['date', 'class','name_bitcoin', 'class_bitcoin'], axis = 1)
y = data['class']
num_feats = 150

def cor_selector(X, y,num_feats):
    cor_list = []
    feature_name = X.columns.tolist()
    # calculate the correlation with y for each feature
    for i in X.columns.tolist():
        cor = np.corrcoef(X[i], y)[0, 1]
        cor_list.append(cor)
    # replace NaN with 0
    cor_list = [0 if np.isnan(i) else i for i in cor_list]
    # feature name
    cor_feature = X.iloc[:,np.argsort(np.abs(cor_list))[-num_feats:]].columns.tolist()
    # feature selection? 0 for not select, 1 for select
    cor_support = [True if i in cor_feature else False for i in feature_name]
    return cor_support, cor_feature
cor_support, cor_feature = cor_selector(X, y,num_feats)


from sklearn.feature_selection import SelectKBest
from sklearn.feature_selection import chi2
from sklearn.preprocessing import MinMaxScaler
X_norm = MinMaxScaler().fit_transform(X)
chi_selector = SelectKBest(chi2, k=num_feats)
chi_selector.fit(X_norm, y)
chi_support = chi_selector.get_support()
chi_feature = X.loc[:,chi_support].columns.tolist()
print(str(len(chi_feature)), 'selected features')


from sklearn.feature_selection import RFE
from sklearn.linear_model import LogisticRegression
rfe_selector = RFE(estimator=LogisticRegression(), n_features_to_select=num_feats, step=10, verbose=5)
rfe_selector.fit(X_norm, y)
rfe_support = rfe_selector.get_support()
rfe_feature = X.loc[:,rfe_support].columns.tolist()
print(str(len(rfe_feature)), 'selected features')

from sklearn.feature_selection import SelectFromModel
from sklearn.linear_model import LogisticRegression

embeded_lr_selector = SelectFromModel(LogisticRegression(penalty="l1"), max_features=num_feats)
embeded_lr_selector.fit(X_norm, y)

embeded_lr_support = embeded_lr_selector.get_support()
embeded_lr_feature = X.loc[:,embeded_lr_support].columns.tolist()
print(str(len(embeded_lr_feature)), 'selected features')


from sklearn.feature_selection import SelectFromModel
from sklearn.ensemble import RandomForestClassifier

embeded_rf_selector = SelectFromModel(RandomForestClassifier(n_estimators=100), max_features=num_feats)
embeded_rf_selector.fit(X, y)

embeded_rf_support = embeded_rf_selector.get_support()
embeded_rf_feature = X.loc[:,embeded_rf_support].columns.tolist()
print(str(len(embeded_rf_feature)), 'selected features')


from sklearn.feature_selection import SelectFromModel
from lightgbm import LGBMClassifier

lgbc=LGBMClassifier(n_estimators=500, learning_rate=0.05, num_leaves=32, colsample_bytree=0.2,
            reg_alpha=3, reg_lambda=1, min_split_gain=0.01, min_child_weight=40)

embeded_lgb_selector = SelectFromModel(lgbc, max_features=num_feats)
embeded_lgb_selector.fit(X, y)

embeded_lgb_support = embeded_lgb_selector.get_support()
embeded_lgb_feature = X.loc[:,embeded_lgb_support].columns.tolist()
print(str(len(embeded_lgb_feature)), 'selected features')


# put all selection together
feature_selection_df = pd.DataFrame({'Feature':feature_name, 'Pearson':cor_support, 'Chi-2':chi_support, 'RFE':rfe_support, 'Logistics':embeded_lr_support,
                                    'Random Forest':embeded_rf_support, 'LightGBM':embeded_lgb_support})
# count the selected times for each feature
feature_selection_df['Total'] = np.sum(feature_selection_df, axis=1)
# display the top 100
feature_selection_df = feature_selection_df.sort_values(['Total','Feature'] , ascending=False)
feature_selection_df.index = range(1, len(feature_selection_df)+1)