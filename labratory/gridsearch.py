from keras.models import Sequential
from keras.layers import Dense
from keras.callbacks import ModelCheckpoint
import numpy as np
import sklearn.preprocessing as prep
from sklearn.preprocessing import StandardScaler
import pandas as pd
import tensorflow as tf
import keras
from keras.datasets import mnist
from keras.models import Sequential
from keras.layers import Dense, Dropout, Flatten
from sklearn.model_selection import GridSearchCV
from keras.wrappers.scikit_learn import KerasClassifier
from keras.optimizers import SGD
from keras.constraints import maxnorm

cb = [ModelCheckpoint("model.hdf5", monitor='val_loss', save_weights_only=False, period=3)]


def preprocess_data(stock, seq_len):
    amount_of_features = len(stock.columns)
    data = stock.values

    # sequence_length = seq_len + 1
    # result = []
    # for index in range(len(data) - sequence_length):
    #     result.append(data[index: index + sequence_length])

    result = np.array(data)
    row = round(0.9 * result.shape[0])
    train = result[: int(row), :]

    scaler = StandardScaler()
    scaler.fit(train)
    train = scaler.transform(train)
    scaler.fit(result)
    result = scaler.transform(result)

    X_train = train[:, : -1]
    y_train = train[:, -1]
    X_test = result[int(row):, : -1]
    y_test = result[int(row):, -1]

    # X_train = np.reshape(X_train, (X_train.shape[0], X_train.shape[1], amount_of_features))
    # X_test = np.reshape(X_test, (X_test.shape[0], X_test.shape[1], amount_of_features))

    return [X_train, y_train, X_test, y_test]


def create_model():
    # default values
    activation = 'relu'  # or linear
    dropout_rate = 0.0  # or 0.2
    init_mode = 'uniform'
    weight_constraint = 0  # or  4
    optimizer = 'adam'  # or SGD
    lr = 0.01
    momemntum = 0
    # create model
    model = Sequential()
    model.add(Dense(8,
                    input_dim=20, kernel_initializer=init_mode,
                    activation=activation,
                    kernel_constraint=maxnorm(weight_constraint)))
    model.add(Dropout(dropout_rate))
    model.add(Dense(1, kernel_initializer=init_mode, activation='sigmoid'))
    # Compile model
    model.compile(loss='mae',
                  optimizer=optimizer,
                  metrics=['accuracy'])
    return model


if __name__ == '__main__':
    df = pd.read_csv('prediction/data/day.csv')
    window = 20
    X_train, y_train, X_test, y_test = preprocess_data(df[:: -1], window)
    print("X_train", X_train.shape)
    print("y_train", y_train.shape)
    print("X_test", X_test.shape)
    print("y_test", y_test.shape)
    # create model
    model = KerasClassifier(build_fn=create_model, batch_size=1000, epochs=10)
    # use verbose=0 if you do not want to see progress

    ########################################################
    # Use scikit-learn to grid search
    activation = ['relu', 'tanh', 'sigmoid', 'hard_sigmoid', 'linear']  # softmax, softplus, softsign
    momentum = [0.0, 0.2, 0.4, 0.6, 0.8, 0.9]
    learn_rate = [0.001, 0.01, 0.1, 0.2, 0.3]
    dropout_rate = [0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9]
    weight_constraint = [1, 2, 3, 4, 5]
    neurons = [1, 5, 10, 15, 20, 25, 30]
    init = ['uniform', 'lecun_uniform', 'normal', 'zero', 'glorot_normal', 'glorot_uniform', 'he_normal', 'he_uniform']
    optimizer = ['SGD', 'RMSprop', 'Adagrad', 'Adadelta', 'Adam', 'Adamax', 'Nadam']
    ##############################################################
    # grid search epochs, batch size
    epochs = [1, 10, 20, 30, 40]  # add 50, 100, 150 etc
    batch_size = [5, 10, 15, 20, 40]  # add 5, 10, 20, 40, 60, 80, 100 etc
    param_grid = dict(epochs=epochs, batch_size=batch_size, activation=activation, momentum=momentum,
                      learn_rate=learn_rate, dropout_rate=dropout_rate, weight_constraint=weight_constraint,
                      neurons=neurons, init=init, optimizer=optimizer)
    ##############################################################
    grid = GridSearchCV(estimator=model, param_grid=param_grid, n_jobs=-1)
    grid_result = grid.fit(X_train, y_train)
    ##############################################################
    # summarize results
    print("Best: %f using %s" % (grid_result.best_score_, grid_result.best_params_))
    means = grid_result.cv_results_['mean_test_score']
    stds = grid_result.cv_results_['std_test_score']
    params = grid_result.cv_results_['params']
    for mean, stdev, param in zip(means, stds, params):
        print("%f (%f) with: %r" % (mean, stdev, param))
