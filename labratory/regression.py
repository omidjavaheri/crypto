import math
import matplotlib
import numpy as np
import pandas as pd
import seaborn as sns
import time

from datetime import date, datetime, time, timedelta
from matplotlib import pyplot as plt
from pylab import rcParams
from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_squared_error
from sklearn.metrics import r2_score
from tqdm import tqdm_notebook

#### Input params ##################
stk_path = "./withDate.csv"
test_size = 0.2                 # proportion of dataset to be used as test set
cv_size = 0.2                   # proportion of dataset to be used as cross-validation set
Nmax = 30                       # for feature at day t, we use lags from t-1, t-2, ..., t-N as features
                                # Nmax is the maximum N we are going to test
fontsize = 14
ticklabelsize = 14
####################################

def target_generator(data):
    b = data['price']
    for in_days in range(1,2):
        target = 'price_in_{}_day'.format(in_days)
        data[target] = data['price']
        a = data[target]
        for i in range(len(b)):
            if i<len(b)-in_days:
                a[i] = b[i+in_days]
            else:
                a[i] = b[i-5:i].mean()

def zero_cleaner(column):
    up =0
    for i in range(len(column)):
        val = column[i]
        if column[i] == 0 and up ==0 and i >=1:
            up = column[i-1]
        if column[i] != 0 and up!=0:
            j=i
            ave = (column[i]+up)/2
            while j>=1 and column[j-1] ==0:
                column[j-1] = ave
                j-=1
            up = 0

def get_preds_lin_reg(df, target_col, N, pred_min, offset):
    """
    Given a dataframe, get prediction at timestep t using values from t-1, t-2, ..., t-N.
    Inputs
        df         : dataframe with the values you want to predict. Can be of any length.
        target_col : name of the column you want to predict e.g. 'adj_close'
        N          : get prediction at timestep t using values from t-1, t-2, ..., t-N
        pred_min   : all predictions should be >= pred_min
        offset     : for df we only do predictions for df[offset:]. e.g. offset can be size of training set
    Outputs
        pred_list  : the predictions for target_col. np.array of length len(df)-offset.
    """
    # Create linear regression object
    regr = LinearRegression(fit_intercept=True)

    pred_list = []

    for i in range(offset, len(df['price'])):
        X_train = np.array(range(len(df['price'][i - N:i])))  # e.g. [0 1 2 3 4]
        y_train = np.array(df['price_in_1_day'][i - N:i])  # e.g. [2944 3088 3226 3335 3436]
        X_train = X_train.reshape(-1, 1)  # e.g X_train =
        # [[0]
        #  [1]
        #  [2]
        #  [3]
        #  [4]]
        # X_train = np.c_[np.ones(N), X_train]              # add a column
        y_train = y_train.reshape(-1, 1)
        #     print X_train.shape
        #     print y_train.shape
        #     print 'X_train = \n' + str(X_train)
        #     print 'y_train = \n' + str(y_train)
        regr.fit(X_train, y_train)  # Train the model
        pred = regr.predict(np.array(N).reshape(1, -1))

        pred_list.append(pred[0][0])  # Predict the footfall using the model

    # If the values are < pred_min, set it to be pred_min
    pred_list = np.array(pred_list)
    pred_list[pred_list < pred_min] = pred_min

    return pred_list


def get_mape(y_true, y_pred):
    """
    Compute mean absolute percentage error (MAPE)
    """
    y_true, y_pred = np.array(y_true), np.array(y_pred)
    return np.mean(np.abs((y_true - y_pred) / y_true)) * 100

df = pd.read_csv(stk_path, sep = ",")

# Convert Date column to datetime
df.loc[:, 'date'] = pd.to_datetime(df['date'],format='%Y-%m-%d')

# Get month of each sample
df['month'] = df['date'].dt.month

# Sort by datetime
df.sort_values(by='date', inplace=True, ascending=True)

zero_cleaner_features =['twitterFollowers','redditSubscribers','forks', 'stars',
       'subscribers', 'totalIssues', 'closedIssues','pullRequestContributors', 'additionsIn4Week', 'deletionIn4Week',
       'commitCount4Weeks']
for feature in zero_cleaner_features:
    zero_cleaner(df[feature])
target_generator(df)

num_cv = int(cv_size*len(df))
num_test = int(test_size*len(df))
num_train = len(df) - num_cv - num_test

# Split into train, cv, and test
train = df[:num_train].copy()
cv = df[num_train:num_train+num_cv].copy()
train_cv = df[:num_train+num_cv].copy()
test = df[num_train+num_cv:].copy()


est_list = get_preds_lin_reg(df, 'price', 4, 0, num_train+num_cv)
test.loc[:, 'est' + '_N' + str(4)] = est_list
print("RMSE = %0.3f" % math.sqrt(mean_squared_error(est_list, test['price'])))
print("R2 = %0.3f" % r2_score(test['price'], est_list))
print("MAPE = %0.3f%%" % get_mape(test['price'], est_list))
rcParams['figure.figsize'] = 10, 8 # width 10, height 8

ax = train.plot(x='date', y='price', style='b-', grid=True)
ax = cv.plot(x='date', y='price', style='y-', grid=True, ax=ax)
ax = test.plot(x='date', y='price', style='g-', grid=True)
ax = test.plot(x='date', y='est_N4', style='r-', grid=True, ax=ax)
ax.legend(['train', 'dev', 'test', 'predictions with N_opt=5'])
ax.set_xlabel("date")
ax.set_ylabel("USD")
plt.show()