from sklearn.ensemble import RandomForestRegressor
from sklearn.preprocessing import MinMaxScaler
import joblib

def fit_and_predict(data):
    
    feat = ['price', 'volume', 'marketCap', 'twitterFollowers',
       'redditAveragePosts48h', 'redditAverageComments48h',
       'redditSubscribers', 'redditAccountsActive48h',
       'telegramChanelUserCount', 'alexaRank', 'bingMatches', 'forks', 'stars',
       'subscribers', 'totalIssues', 'closedIssues', 'pullRequestsMerged',
       'pullRequestContributors', 'additionsIn4Week', 'deletionIn4Week',
       'commitCount4Weeks']
        
    regr = joblib.load("./alpha_model_v1_(7.8.2020).joblib")

    min_max_scaler = MinMaxScaler()
    min_max_scaler.fit(data[['price']])
    data['price'] = min_max_scaler.transform(data[['price']])
    
    test = data.tail(1)[feat]
    
    pred = regr.predict(test)
    pred = pred[0]
    
    today_price = data.iloc[-1]['price']
    today_price = min_max_scaler.inverse_transform(today_price.reshape(1, -1))[0][0]
    tomorrow_price = pred
    tomorrow_price = min_max_scaler.inverse_transform(tomorrow_price.reshape(1, -1))[0][0]
    tomorrow_percentage = ((tomorrow_price - today_price) / abs(today_price)) * 100
    return {'today_price': today_price, 'tomorrow_price': tomorrow_price, 'tomorrow_percentage': tomorrow_percentage}
   