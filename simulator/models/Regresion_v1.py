from simulator.models.BaseModel import BaseModel
import xgboost as xgb

class Regression_v1(BaseModel):
    def preprocess(self):



    def predict(self):
        dtrain = xgb.DMatrix(self.X_train, label=self.y_train)
        dtest = xgb.DMatrix(self.X_test, label=self.y_test)

        # set xgboost params
        param = {
            'max_depth': 20,
            'eta': 0.1,
            'objective': 'reg:squarederror',
        }
        num_round = 50

        best = xgb.train(param, dtrain, num_round)
        return best.predict(dtest)

