from simulator.models.BaseModel import BaseModel

class LastValue(BaseModel):
    def predict(self):
        return self.X_test['price']

