from simulator.Simulator import get_mape, get_rmse


class BaseModel:
    def __init__(self, X_train, X_test, y_train, y_test):
        self.model = None
        self.X_train = X_train
        self.X_test = X_test
        self.y_train = y_train
        self.y_test = y_test

    def preprocess(self):
        print(1)
        pass

    def fit(self):
        print(2)
        pass

    def predict(self):
        print(3)
        pass

    def run(self):
        self.preprocess()
        self.fit()
        pred = self.predict()
        return self.profit(pred), self.eval(pred)

    def eval(self, pred):
        mape = get_mape(pred, self.y_test)
        rmse = get_rmse(pred, self.y_test)
        return mape, rmse

    def profit(self, pred):
        cast = pred.values
        true = self.y_test.values
        bank = 100
        coin = 0
        for i, elm in enumerate(cast):
            if i == cast.shape[0] - 1:
                continue
            diff = cast[i + 1] - cast[i]
            if diff < 0 and coin != 0:
                bank += coin - true[i]
                coin = 0
            if diff > 0 and coin == 0:
                coin = true[i]
            # print(cast[i], '\t', true[i], '\t', diff,  '\t', bank)
        return bank
