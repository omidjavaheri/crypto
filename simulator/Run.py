from simulator.Simulator import *
from simulator.models.LastValue import LastValue

data = load_data('./data/neo_full.csv')
prep_data(data)
diff_data = diff_data(data)
X_train, X_test, y_train, y_test = train_test_split(data=data,i=900,target_col='price_in_1_day')
X_train_diff, X_test_diff, y_train_diff, y_test_diff = train_test_split(data=diff_data,i=900,target_col='price_in_1_day')
print(X_train.shape, X_test.shape, y_train.shape, y_test.shape)
print(X_train_diff.shape, X_test_diff.shape, y_train_diff.shape, y_test_diff.shape)


models = []
models.append(['last_value', LastValue(X_train, X_test, y_train, y_test)])

for model in models:
    print(model[0])
    print(model[1].run())


