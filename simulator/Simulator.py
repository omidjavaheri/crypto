import math
import warnings

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from sklearn.decomposition import PCA

warnings.filterwarnings("ignore")

zero_cleaner_features = ['marketCap', 'twitterFollowers', 'redditSubscribers', 'forks', 'stars',
                         'subscribers', 'totalIssues', 'closedIssues', 'pullRequestContributors', 'additionsIn4Week', 'deletionIn4Week',
                         'commitCount4Weeks']
fill_nan = ['marketCap']
features = ['price', 'volume', 'marketCap', 'twitterFollowers',
            'redditAveragePosts48h', 'redditAverageComments48h',
            'redditSubscribers', 'redditAccountsActive48h',
            'alexaRank', 'forks', 'stars',
            'subscribers', 'totalIssues', 'closedIssues',
            'pullRequestContributors', 'additionsIn4Week', 'deletionIn4Week',
            'commitCount4Weeks']
targets = ['price_in_1_day', 'price_in_2_day',
           'price_in_3_day', 'price_in_4_day', 'price_in_5_day', 'price_in_6_day',
           'price_in_7_day', 'price_in_8_day', 'price_in_9_day', 'price_in_10_day',
           'grow_in_1_day', 'grow_in_2_day', 'grow_in_3_day', 'grow_in_4_day',
           'grow_in_5_day', 'grow_in_6_day', 'grow_in_7_day', 'grow_in_8_day',
           'grow_in_9_day', 'grow_in_10_day']


def zero_cleaner(column):
    up = 0
    for i in range(len(column)):
        val = column[i]
        if column[i] == 0 and up == 0 and i >= 1:
            up = column[i - 1]
        if column[i] != 0 and up != 0:
            j = i
            ave = (column[i] + up) / 2
            while j >= 1 and column[j - 1] == 0:
                column[j - 1] = ave
                j -= 1
            up = 0


def target_generator(data, col='price'):
    b = data[col]
    for in_days in range(1, 11):
        target = 'price_in_{}_day'.format(in_days)
        data[target] = data[col]
        a = data[target]
        for i in range(len(b)):
            if i < len(b) - in_days:
                a[i] = b[i + in_days]
            else:
                a[i] = b[i - 5:i].mean()


def grow_label_generator(data):
    b = data['price']
    for in_days in range(1, 11):
        target = 'grow_in_{}_day'.format(in_days)
        data[target] = data['price']
        a = data[target]
        for i in range(len(b)):
            if i < len(b) - in_days:
                if a[i] < b[i + in_days]:
                    a[i] = 1
                else:
                    a[i] = 0
            else:
                a[i] = -1


def get_mape(y_true, y_pred):
    """
    Compute mean absolute percentage error (MAPE)
    """
    y_true, y_pred = np.array(y_true), np.array(y_pred)
    return np.mean(np.abs((y_true - y_pred) / y_true)) * 100


def get_rmse(y_true, y_pred):
    """
    Comp RMSE. a and b can be lists.
    Returns a scalar.
    """
    return math.sqrt(np.mean((np.array(y_true) - np.array(y_pred)) ** 2))


def plot_trend(true_trend, pred_trend=[], split_point=0, full_x=False, title=''):
    plt.figure(num=None, figsize=(20, 6), dpi=80, facecolor='w', edgecolor='k')
    plt.plot(true_trend, 'blue', label="true values", alpha=0.6)
    if len(pred_trend) > 0:
        plt.plot(pred_trend, 'red', label='prediction', alpha=0.6)
    plt.legend(loc="upper left")
    plt.title(title)
    if full_x == True:
        plt.xticks(range(len(true_trend)))
        plt.grid(True)
    if split_point != 0:
        plt.axvline(x=split_point, linewidth=2.5)
    plt.show()


def prep_data(data):
    for feature in fill_nan:
        data[feature] = data[feature].fillna(0)
    for feature in zero_cleaner_features:
        zero_cleaner(data[feature])
    normalize_price(data)
    target_generator(data, col='normalized_price')
    # grow_label_generator(data)


def normalize_price(data):
    c = data['price']
    g = data['goldPrice']
    normalized = []
    for i in range(len(c)):
        if i > 0:
            rc = (c[i] - c[i - 1]) / c[i - 1]
            rg = (g[i] - g[i - 1]) / g[i - 1]
            normalized.append((1 + rc) / (1 + rg))
        elif i == 0:
            normalized.append(1)
    data['normalized_price'] = normalized


def diff_data(data):
    cols = ['price', 'volume', 'marketCap', 'twitterFollowers',
            'redditAveragePosts48h', 'redditAverageComments48h',
            'redditSubscribers', 'redditAccountsActive48h',
            'telegramChanelUserCount', 'alexaRank', 'bingMatches', 'forks', 'stars',
            'subscribers', 'totalIssues', 'closedIssues', 'pullRequestsMerged',
            'pullRequestContributors', 'additionsIn4Week', 'deletionIn4Week',
            'commitCount4Weeks']
    for i, row in data.iterrows():
        for col in cols:
            for hor in range(1, 50):
                try:
                    data.at[i, 'diff_{}_in_{}_day'.format(col, hor)] = data.at[i, col] - data.at[i - hor, col]
                except:
                    pass

    temp = data.tail(data.shape[0] - 50)
    c = temp.columns.values.tolist()
    cc = ['name', 'date', 'price_in_1_day', 'price_in_2_day', 'price_in_3_day', 'price_in_4_day', 'price_in_5_day', 'price_in_6_day',
          'price_in_7_day', 'price_in_8_day', 'price_in_9_day', 'price_in_10_day']

    col = [x for x in c if x not in cc]

    temp = temp[col]
    pca_data = pd.DataFrame(PCA(n_components=60).fit_transform(temp))
    pca_data['target'] = data.tail(data.shape[0] - 50)['price_in_1_day'].values - data.tail(data.shape[0] - 50)['price'].values
    pca_data['price'] = data.tail(data.shape[0] - 50)['price'].values
    pca_data['price_in_1_day'] = data.tail(data.shape[0] - 50)['price_in_1_day'].values
    return pca_data


def load_data(path):
    return pd.read_csv(path)


def save_data(path, data):
    data.to_csv(path)


def train_test_split(data, i, target_col):
    train = data[:i]
    test = data[i:]
    X_train = train[features]
    y_train = train[target_col]
    X_test = test[features]
    y_test = test[target_col]
    return X_train, X_test, y_train, y_test


def train_test_split_diff(data, i, target_col):
    train = data[:i]
    test = data[i:]
    X_train = train[features]
    y_train = train[target_col]
    X_test = test[features]
    y_test = test[target_col]
    return X_train, X_test, y_train, y_test
