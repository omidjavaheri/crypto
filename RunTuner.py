
import psycopg2
from psycopg2 import pool

from tuner.models.boost import Boost
from tuner.models.lstm import Lstm
from tuner.models.regression import Regression
from tuner.models.rnn import Rnn
import os
import pandas as pd

postgreSQL_pool = psycopg2.pool.SimpleConnectionPool(1, 20, user="postgres",
                                                     password="9c2394ffb07e850f5e93875d6a70487c20b71a4cd388d584",
                                                     host="postgres_pred",
                                                     port="5432",
                                                     database="coins")

THIS_FOLDER = os.path.dirname(os.path.abspath(__file__))
my_file = os.path.join(THIS_FOLDER, 'tuner/data/day.csv')
df = pd.read_csv(my_file)
datas = df.groupby('name')
for name, data in datas:
    # [:-1] is removing last record which it's data is not complete and affects tune result
    model = Lstm(data._get_numeric_data()[:-1], name, postgreSQL_pool)
    try:
        model.run()
    except:
        pass
    model = Boost(data._get_numeric_data()[:-1], name, postgreSQL_pool)
    try:
        model.run()
    except:
        pass
    model = Regression(data._get_numeric_data()[:-1], name, postgreSQL_pool)
    try:
        model.run()
    except:
        pass
    model = Rnn(data._get_numeric_data()[:-1], name, postgreSQL_pool)
    try:
        model.run()
    except:
        pass