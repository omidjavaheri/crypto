import datetime
import os
import threading

import pandas as pd
import psycopg2
from psycopg2 import pool

from prediction.tuned.avg import Avg
from prediction.tuned.boost import MyBoost
from prediction.tuned.lstm import Lstm
from prediction.tuned.rnn import Rnn
from prediction.tuned.regression import Regression


THIS_FOLDER = os.path.dirname(os.path.abspath(__file__))
my_file = os.path.join(THIS_FOLDER, 'prediction/data/day.csv')
df = pd.read_csv(my_file)
# df = df.loc[df['name'] == '0x (zrx)']
# df = df[df['date'] < '2020-10-08 01:00:00.0']
datas = df.groupby('name')
result = {}
postgreSQL_pool = psycopg2.pool.SimpleConnectionPool(1, 20, user="postgres",
                                                     password="123456",
                                                     host="127.0.0.1",
                                                     port="5432",
                                                     database="coins")
def create_table():
    create_table_query = '''CREATE TABLE model_of_models (
        id bigserial primary key,
        target_date timestamp default NULL,
        coin text default NULL,
        lstm text default NULL,
        xgboost_rmse text default NULL,
        xgboost_mape text default NULL,
        regression4 text default NULL,
        regression5 text default NULL,
        regressionN_rmse text default NULL,
        regressionN_mape text default NULL,
        avg text default NULL,
        rnn text default NULL,
        label text default NULL
    ); '''
    con = None
    cur = None
    try:
        con = postgreSQL_pool.getconn()
        cur = con.cursor()
        cur.execute("select * from information_schema.tables where table_name=%s", ('model_of_models',))
        if bool(cur.rowcount) is False:
            cur.execute(create_table_query)
            con.commit()
    finally:
        postgreSQL_pool.putconn(con)

def create_record(coin, target_date, label):
    connection = None
    cursor = None
    try:
        connection = postgreSQL_pool.getconn()
        cursor = connection.cursor()
        postgres_insert_query = """ INSERT INTO model_of_models (coin, target_date, label) 
                                    VALUES (%s,%s,%s)"""
        record_to_insert = (coin, target_date, label)
        cursor.execute(postgres_insert_query, record_to_insert)
        connection.commit()
    finally:
        postgreSQL_pool.putconn(connection)

def update_record(coin, model, predicted, target_date):
    connection = None
    cursor = None
    try:
        connection = postgreSQL_pool.getconn()
        cursor = connection.cursor()
        postgres_insert_query = "update model_of_models set {}=%s where coin=%s and target_date=%s".format(model)
        record_to_insert = (predicted, coin, target_date)
        cursor.execute(postgres_insert_query, record_to_insert)
        connection.commit()
    finally:
        postgreSQL_pool.putconn(connection)

def record_exist(coin, target_date):
    con = None
    try:
        con = postgreSQL_pool.getconn()
        cur = con.cursor()
        cur.execute("select 1 from model_of_models where coin=%s and target_date=%s", (coin, target_date))
        return bool(cur.rowcount)
    finally:
        postgreSQL_pool.putconn(con)

def model_has_been_predicted(coin, model, target_date):
    con = None
    try:
        con = postgreSQL_pool.getconn()
        cur = con.cursor()
        cur.execute("select 1 from model_of_models where coin=%s and target_date=%s and {} is not null".format(model), (coin, target_date))
        return bool(cur.rowcount)
    finally:
        postgreSQL_pool.putconn(con)

def fetch_parameters(query):
    con = None
    try:
        con = postgreSQL_pool.getconn()
        cur = con.cursor()
        cur.execute(query)
        return cur.fetchall()
    finally:
        postgreSQL_pool.putconn(con)

def processEachCoin(name, data, label):
    result[name] = {}
    models = {}
    today_date = data.iloc[-1, data.columns.get_loc('date')]
    today_date = datetime.datetime.strptime(today_date, "%Y-%m-%d %H:%M:%S.%f")
    target_date = today_date + datetime.timedelta(days=1)
    if not record_exist(name, target_date):
        create_record(name, target_date, '1' if label > 0 else '0')
    if not model_has_been_predicted(name, 'lstm', target_date):
        query = "select N, units, dropout, optimizer, epochs, batch_size from lstm where coin='{}'".format(name)
        params = fetch_parameters(query)
        models.update({'lstm': Lstm(data._get_numeric_data(), name, params)})
    if not model_has_been_predicted(name, 'xgboost_rmse', target_date):
        query = "select n_estimators, max_depth, learning_rate, min_child_weight, subsample, colsample_bytree, colsample_bylevel, gamma from boost where coin='{}' and error_type='RMSE'".format(name)
        params = fetch_parameters(query)
        models.update({'xgboost_rmse': MyBoost(data._get_numeric_data(), name, params)})
    if not model_has_been_predicted(name, 'xgboost_mape', target_date):
        query = "select n_estimators, max_depth, learning_rate, min_child_weight, subsample, colsample_bytree, colsample_bylevel, gamma from boost where coin='{}' and error_type='MAPE'".format(name)
        params = fetch_parameters(query)
        models.update({'xgboost_mape': MyBoost(data._get_numeric_data(), name, params)})
    if not model_has_been_predicted(name, 'regression4', target_date):
        models.update({'regression4': Regression(data._get_numeric_data(), name, [[4]])})
    if not model_has_been_predicted(name, 'regression5', target_date):
        models.update({'regression5': Regression(data._get_numeric_data(), name, [[5]])})
    if not model_has_been_predicted(name, 'regressionN_rmse', target_date):
        query = "select N from regression where coin='{}' and error_type='RMSE'".format(name)
        params = fetch_parameters(query)
        models.update({'regressionN_rmse': Regression(data._get_numeric_data(), name, params)})
    if not model_has_been_predicted(name, 'regressionN_mape', target_date):
        query = "select N from regression where coin='{}' and error_type='MAPE'".format(name)
        params = fetch_parameters(query)
        models.update({'regressionN_mape': Regression(data._get_numeric_data(), name, params)})
    if not model_has_been_predicted(name, 'avg', target_date):
        models.update({'avg': Avg(data._get_numeric_data(), name, None)})
    if not model_has_been_predicted(name, 'rnn', target_date):
        query = "select N, units, dropout, optimizer, epochs, batch_size from rnn where coin='{}'".format(name)
        params = fetch_parameters(query)
        models.update({'rnn': Rnn(data._get_numeric_data(), name, params)})
    for k, v in models.items():
        try:
            x = v.run()
            update_record(name, k, '1' if (x['tomorrow_price'] - x['today_price']) > 0 else '0', target_date)
        except Exception as e:
            print("coin : " + name + " and model : " + k + " has this exception : " + str(e))



def paralle_process_chunks():
    for item in groups:
        threads.append(threading.Thread(target=processEachCoin, args=(item['name'], item['data'], item['label'])))
    for thread in threads:
        thread.start()
    for thread in threads:
        thread.join()


groups = []
threads = []
chunks = 1
index = 0
create_table()
for name, data in datas:
    index = index + 1
    test_size = round(0.3 * len(data))
    data_size = len(data)
    if index % chunks == 0:
        for i in range(test_size, 0, -1):
            try:
                today_price = data.iloc[data_size-i-1, data.columns.get_loc('price')]
                tomorrow_price = data.iloc[data_size-i, data.columns.get_loc('price')]
                groups.append({'name': name, 'data': data[:data_size-i], 'label': tomorrow_price - today_price})
                paralle_process_chunks()
                threads = []
                groups = []
            except Exception as e:
                print(str(e))

if len(groups) > 0:
    paralle_process_chunks()



