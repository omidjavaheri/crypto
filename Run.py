import datetime
import os
import threading
import warnings

import pandas as pd
import psycopg2
from psycopg2 import pool

from prediction.models.avg import Avg
from prediction.models.boost import MyBoost
from prediction.models.lstm import Lstm
from prediction.models.regression import Regression
from prediction.models.rnn import Rnn

import smtplib
from os.path import basename
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.utils import formatdate
import csv


THIS_FOLDER = os.path.dirname(os.path.abspath(__file__))
my_file = os.path.join(THIS_FOLDER, 'prediction/data/day.csv')
df = pd.read_csv(my_file)
datas = df.groupby('name')
result = {}
tomorrow_date_to_create_predict_file = None
there_is_new_prediction = False
postgreSQL_pool = psycopg2.pool.SimpleConnectionPool(1, 20, user="postgres",
                                                     password="9c2394ffb07e850f5e93875d6a70487c20b71a4cd388d584",
                                                     host="postgres_pred",
                                                     port="5432",
                                                     database="coins")
def create_table():
    create_table_query = '''CREATE TABLE pred (
        id bigserial primary key,
        coin text NOT NULL,
        model text NOT NULL,
        today_price text NOT NULL,
        tomorrow_price text NOT NULL,
        tomorrow_percentage text NOT NULL,
        tomorrow_date timestamp default NULL
    ); '''
    con = None
    cur = None
    try:
        con = postgreSQL_pool.getconn()
        cur = con.cursor()
        cur.execute("select * from information_schema.tables where table_name=%s", ('pred',))
        if bool(cur.rowcount) is False:
            cur.execute(create_table_query)
            con.commit()
    finally:
        postgreSQL_pool.putconn(con)

def insert_into_db(coin, model, today_price, tomorrow_price, tomorrow_percentage, tomorrow_date):
    connection = None
    cursor = None
    try:
        connection = postgreSQL_pool.getconn()
        cursor = connection.cursor()
        postgres_insert_query = """ INSERT INTO pred (coin, model, today_price, tomorrow_price, tomorrow_percentage, tomorrow_date) 
                                    VALUES (%s,%s,%s,%s,%s,%s)"""
        record_to_insert = (coin, model, str(today_price), str(tomorrow_price), str(tomorrow_percentage), tomorrow_date)
        cursor.execute(postgres_insert_query, record_to_insert)
        connection.commit()
    finally:
        postgreSQL_pool.putconn(connection)

def exist(coin, model, tomorrow_date):
    con = None
    try:
        con = postgreSQL_pool.getconn()
        cur = con.cursor()
        cur.execute("select 1 from pred where coin=%s and model=%s and tomorrow_date=%s", (coin, model, tomorrow_date))
        return bool(cur.rowcount)
    finally:
        postgreSQL_pool.putconn(con)

def create_pred_file():
    con = None
    try:
        con = postgreSQL_pool.getconn()
        cur = con.cursor()
        cur.execute("select * from pred where cast(tomorrow_date as varchar) like %s", (tomorrow_date_to_create_predict_file.strftime('%Y-%m-%d') + '%',))
        rows = cur.fetchall()
        THIS_FOLDER = os.path.dirname(os.path.abspath(__file__))
        f = os.path.join(THIS_FOLDER, "prediction/data/pred.csv")
        fp = open(f, 'w')
        myFile = csv.writer(fp)
        myFile.writerows(rows)
        fp.close()
    finally:
        postgreSQL_pool.putconn(con)

def processEachCoin(name, data):
    result[name] = {}
    models = {}
    today_date = data.iloc[-1, data.columns.get_loc('date')]
    tomorrow_date = datetime.datetime.strptime(today_date, "%Y-%m-%d %H:%M:%S.%f")
    tomorrow_date = tomorrow_date + datetime.timedelta(days=1)
    if not exist(name, 'lstm', tomorrow_date):
        global tomorrow_date_to_create_predict_file
        tomorrow_date_to_create_predict_file = tomorrow_date
        global there_is_new_prediction
        there_is_new_prediction = True
        models.update({'lstm': Lstm(data._get_numeric_data(), name)})
    if not exist(name, 'xgboost', tomorrow_date):
        models.update({'xgboost': MyBoost(data._get_numeric_data(), name)})
    if not exist(name, 'regression', tomorrow_date):
        models.update({'regression': Regression(data._get_numeric_data(), name)})
    if not exist(name, 'avg', tomorrow_date):
        models.update({'avg': Avg(data._get_numeric_data(), name)})
    if not exist(name, 'rnn', tomorrow_date):
        models.update({'rnn': Rnn(data._get_numeric_data(), name)})
    for k, v in models.items():
        try:
            x = v.run()
            insert_into_db(name, k, x['today_price'], x['tomorrow_price'], x['tomorrow_percentage'], tomorrow_date)
        except:
            pass
        # lock.acquire()
        # try:
        #     result[name].update({k: {'today_price': x['today_price'], 'tomorrow_price': x['tomorrow_price'],
        #                              'tomorrow_percentage': x['tomorrow_percentage'],
        #                              'tomorrow_date': tomorrow_date}})
        # finally:
        #     lock.release()


threads = []


def paralle_process_chunks():
    for item in groups:
        threads.append(threading.Thread(target=processEachCoin, args=(item['name'], item['data'])))
    for thread in threads:
        thread.start()
    for thread in threads:
        thread.join()


def send_mail(send_from, send_to, subject, text, files=None):
    assert isinstance(send_to, list)

    msg = MIMEMultipart()
    msg['From'] = send_from
    msg['To'] = ', '.join(send_to)
    msg['Date'] = formatdate(localtime=True)
    msg['Subject'] = subject

    msg.attach(MIMEText(text))

    for f in files or []:
        THIS_FOLDER = os.path.dirname(os.path.abspath(__file__))
        f = os.path.join(THIS_FOLDER, f)
        with open(f, "rb") as fil:
            part = MIMEApplication(
                fil.read(),
                Name=basename(f)
            )
        # After the file is closed
        part['Content-Disposition'] = 'attachment; filename="%s"' % basename(f)
        msg.attach(part)


    smtp = smtplib.SMTP('smtp.gmail.com', 587)
    smtp.starttls()
    smtp.login("coindooni@gmail.com", "Pelak135@Vahede2")
    smtp.sendmail(send_from, send_to, msg.as_string())
    smtp.close()


chunks = 1
index = 0
groups = []
create_table()
for name, data in datas:
    groups.append({'name': name, 'data': data})
    index = index + 1
    if index % chunks == 0:
        paralle_process_chunks()
        threads = []
        groups = []

if len(groups) > 0:
    paralle_process_chunks()

if there_is_new_prediction:
    create_pred_file()
    send_mail("coindooni@gmail.com", ["omid.javaheri66@gmail.com", "maysammoeini@gmail.com", "market.hosseini@gmail.com", "verdino1102@gmail.com"], tomorrow_date_to_create_predict_file.strftime('%Y-%m-%d'), "no body", ['prediction/data/pred.csv'])

# data_frame = pd.DataFrame(
#     columns=['coin', 'model', 'today_price', 'tomorrow_price', 'tomorrow_percentage', 'tomorrow_date'])
# for coin, models in result.items():
#     for model_name, prediction in models.items():
#         new_row = {'coin': coin, 'model': model_name, 'today_price': prediction['today_price'],
#                    'tomorrow_price': prediction['tomorrow_price'],
#                    'tomorrow_percentage': prediction['tomorrow_percentage'],
#                    'tomorrow_date': prediction['tomorrow_date']}
#         data_frame = data_frame.append(new_row, ignore_index=True)
#
# data_frame = data_frame.sort_values('tomorrow_percentage', ascending=True)
#
# outname = 'result.csv'
# outdir = './result'
# if not os.path.exists(outdir):
#     os.mkdir(outdir)
# fullname = os.path.join(outdir, outname)
# data_frame.to_csv(fullname, index_label='index')

