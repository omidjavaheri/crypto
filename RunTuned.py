import datetime
import os
import threading
import warnings

import pandas as pd
import psycopg2
from psycopg2 import pool

from prediction.tuned.avg import Avg
from prediction.tuned.boost import MyBoost
from prediction.tuned.lstm import Lstm
from prediction.tuned.rnn import Rnn
from prediction.tuned.regression import Regression
from prediction.tuned.alpha import Alpha
from prediction.tuned.beta import Beta
from prediction.tuned.forest import Forest

import smtplib
from os.path import basename
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.utils import formatdate
import csv
from labratory.Graphical_Report import GraphicalReport


THIS_FOLDER = os.path.dirname(os.path.abspath(__file__))
my_file = os.path.join(THIS_FOLDER, 'prediction/data/day.csv')
df = pd.read_csv(my_file)
# df = df.loc[df['name'] == '0x (zrx)']
# df = df[df['date'] < '2020-10-08 01:00:00.0']
today_date = df.iloc[-1, df.columns.get_loc('date')]
tomorrow_date = datetime.datetime.strptime(today_date, "%Y-%m-%d %H:%M:%S.%f")
tomorrow_date = tomorrow_date + datetime.timedelta(days=1)
datas = df.groupby('name')
result = {}
postgreSQL_pool = psycopg2.pool.SimpleConnectionPool(1, 20, user="postgres",
                                                     password="9c2394ffb07e850f5e93875d6a70487c20b71a4cd388d584",
                                                     host="postgres_pred",
                                                     port="5432",
                                                     database="coins")
def create_table():
    create_table_query = '''CREATE TABLE pred (
        id bigserial primary key,
        coin text NOT NULL,
        model text NOT NULL,
        today_price text NOT NULL,
        tomorrow_price text NOT NULL,
        tomorrow_percentage text NOT NULL,
        tomorrow_date timestamp default NULL
    ); '''
    con = None
    cur = None
    try:
        con = postgreSQL_pool.getconn()
        cur = con.cursor()
        cur.execute("select * from information_schema.tables where table_name=%s", ('pred',))
        if bool(cur.rowcount) is False:
            cur.execute(create_table_query)
            con.commit()
    finally:
        postgreSQL_pool.putconn(con)

def create_email_status_table():
    create_table_query = '''CREATE TABLE email_status (
        id bigserial primary key,
        has_sent boolean default false,
        tomorrow_date timestamp default NULL
    ); '''
    con = None
    cur = None
    try:
        con = postgreSQL_pool.getconn()
        cur = con.cursor()
        cur.execute("select * from information_schema.tables where table_name=%s", ('email_status',))
        if bool(cur.rowcount) is False:
            cur.execute(create_table_query)
            con.commit()
    finally:
        postgreSQL_pool.putconn(con)

def insert_into_db(coin, model, today_price, tomorrow_price, tomorrow_percentage, tomorrow_date):
    connection = None
    cursor = None
    try:
        connection = postgreSQL_pool.getconn()
        cursor = connection.cursor()
        postgres_insert_query = """ INSERT INTO pred (coin, model, today_price, tomorrow_price, tomorrow_percentage, tomorrow_date) 
                                    VALUES (%s,%s,%s,%s,%s,%s)"""
        record_to_insert = (coin, model, str(today_price), str(tomorrow_price), str(tomorrow_percentage), tomorrow_date)
        cursor.execute(postgres_insert_query, record_to_insert)
        connection.commit()
    finally:
        postgreSQL_pool.putconn(connection)

def exist(coin, model, tomorrow_date):
    con = None
    try:
        con = postgreSQL_pool.getconn()
        cur = con.cursor()
        cur.execute("select 1 from pred where coin=%s and model=%s and tomorrow_date=%s", (coin, model, tomorrow_date))
        return bool(cur.rowcount)
    finally:
        postgreSQL_pool.putconn(con)

def tomorrow_prediction_email_is_sent(tomorrow_date):
    con = None
    try:
        con = postgreSQL_pool.getconn()
        cur = con.cursor()
        cur.execute("select 1 from email_status where tomorrow_date=%s and has_sent=true", (tomorrow_date,))
        return bool(cur.rowcount)
    finally:
        postgreSQL_pool.putconn(con)

def update_email_satus_to_sent(tomorrow_date):
    con = None
    try:
        con = postgreSQL_pool.getconn()
        cur = con.cursor()
        cur.execute("select 1 from email_status where tomorrow_date=%s", (tomorrow_date,))
        if bool(cur.rowcount):
            cur.execute("update email_status set has_sent=true where tomorrow_date=%s", (tomorrow_date,))
        else:
            cur.execute("insert into email_status (tomorrow_date, has_sent) values (%s, true ) ", (tomorrow_date,))
        con.commit()
    finally:
        postgreSQL_pool.putconn(con)



def fetch_parameters(query):
    con = None
    try:
        con = postgreSQL_pool.getconn()
        cur = con.cursor()
        cur.execute(query)
        return cur.fetchall()
    finally:
        postgreSQL_pool.putconn(con)

def create_pred_file():
    con = None
    try:
        con = postgreSQL_pool.getconn()
        cur = con.cursor()
        cur.execute("select * from pred where cast(tomorrow_date as varchar) like %s", (tomorrow_date.strftime('%Y-%m-%d') + '%',))
        rows = cur.fetchall()
        THIS_FOLDER = os.path.dirname(os.path.abspath(__file__))
        f = os.path.join(THIS_FOLDER, "prediction/data/pred.csv")
        fp = open(f, 'w')
        myFile = csv.writer(fp)
        myFile.writerows(rows)
        fp.close()
    finally:
        postgreSQL_pool.putconn(con)

def processEachCoin(name, data):
    result[name] = {}
    models = {}
    if not exist(name, 'lstm', tomorrow_date):
        query = "select N, units, dropout, optimizer, epochs, batch_size from lstm where coin='{}'".format(name)
        params = fetch_parameters(query)
        models.update({'lstm': Lstm(data._get_numeric_data(), name, params)})
    if not exist(name, 'xgboost_rmse', tomorrow_date):
        query = "select n_estimators, max_depth, learning_rate, min_child_weight, subsample, colsample_bytree, colsample_bylevel, gamma from boost where coin='{}' and error_type='RMSE'".format(name)
        params = fetch_parameters(query)
        models.update({'xgboost_rmse': MyBoost(data._get_numeric_data(), name, params)})
    if not exist(name, 'xgboost_mape', tomorrow_date):
        query = "select n_estimators, max_depth, learning_rate, min_child_weight, subsample, colsample_bytree, colsample_bylevel, gamma from boost where coin='{}' and error_type='MAPE'".format(name)
        params = fetch_parameters(query)
        models.update({'xgboost_mape': MyBoost(data._get_numeric_data(), name, params)})
    if not exist(name, 'regression4', tomorrow_date):
        models.update({'regression4': Regression(data._get_numeric_data(), name, [[4]])})
    if not exist(name, 'regression5', tomorrow_date):
        models.update({'regression5': Regression(data._get_numeric_data(), name, [[5]])})
    if not exist(name, 'regressionN_rmse', tomorrow_date):
        query = "select N from regression where coin='{}' and error_type='RMSE'".format(name)
        params = fetch_parameters(query)
        models.update({'regressionN_rmse': Regression(data._get_numeric_data(), name, params)})
    if not exist(name, 'regressionN_mape', tomorrow_date):
        query = "select N from regression where coin='{}' and error_type='MAPE'".format(name)
        params = fetch_parameters(query)
        models.update({'regressionN_mape': Regression(data._get_numeric_data(), name, params)})
    if not exist(name, 'avg', tomorrow_date):
        models.update({'avg': Avg(data._get_numeric_data(), name, None)})
    if not exist(name, 'alpha', tomorrow_date):
        models.update({'alpha': Alpha(data._get_numeric_data(), name, None)})
    if not exist(name, 'beta', tomorrow_date):
        models.update({'beta': Beta(data._get_numeric_data(), name, None)})
    if not exist(name, 'forest', tomorrow_date):
        models.update({'forest': Forest(data._get_numeric_data(), name, None)})
    if not exist(name, 'rnn', tomorrow_date):
        query = "select N, units, dropout, optimizer, epochs, batch_size from rnn where coin='{}'".format(name)
        params = fetch_parameters(query)
        models.update({'rnn': Rnn(data._get_numeric_data(), name, params)})
    for k, v in models.items():
        try:
            x = v.run()
            insert_into_db(name, k, x['today_price'], x['tomorrow_price'], x['tomorrow_percentage'], tomorrow_date)
        except:
            pass

def paralle_process_chunks():
    for item in groups:
        threads.append(threading.Thread(target=processEachCoin, args=(item['name'], item['data'])))
    for thread in threads:
        thread.start()
    for thread in threads:
        thread.join()


def send_mail(send_from, send_to, subject, text, files=None):
    assert isinstance(send_to, list)

    msg = MIMEMultipart()
    msg['From'] = send_from
    msg['To'] = ', '.join(send_to)
    msg['Date'] = formatdate(localtime=True)
    msg['Subject'] = subject

    msg.attach(MIMEText(text))

    for f in files or []:
        THIS_FOLDER = os.path.dirname(os.path.abspath(__file__))
        f = os.path.join(THIS_FOLDER, f)
        with open(f, "rb") as fil:
            part = MIMEApplication(
                fil.read(),
                Name=basename(f)
            )
        # After the file is closed
        part['Content-Disposition'] = 'attachment; filename="%s"' % basename(f)
        msg.attach(part)


    smtp = smtplib.SMTP('smtp.gmail.com', 587)
    smtp.starttls()
    smtp.login("coindooni@gmail.com", "Pelak135@Vahede2")
    smtp.sendmail(send_from, send_to, msg.as_string())
    smtp.close()


def create_and_send_report():
    con = postgreSQL_pool.getconn()
    try:
        GraphicalReport().graphical_report(tomorrow_date - datetime.timedelta(days=1), con)
    finally:
        postgreSQL_pool.putconn(con)



create_email_status_table()
groups = []
threads = []
if not tomorrow_prediction_email_is_sent(tomorrow_date):
    chunks = 1
    index = 0
    create_table()
    for name, data in datas:
        groups.append({'name': name, 'data': data})
        index = index + 1
        if index % chunks == 0:
            paralle_process_chunks()
            threads = []
            groups = []

    if len(groups) > 0:
        paralle_process_chunks()

    create_pred_file()
    send_mail("coindooni@gmail.com", ["omid.javaheri66@gmail.com", "maysammoeini@gmail.com", "market.hosseini@gmail.com", "verdino1102@gmail.com"], tomorrow_date.strftime('%Y-%m-%d'), "no body", ['prediction/data/pred.csv'])
    update_email_satus_to_sent(tomorrow_date)
    create_and_send_report()



